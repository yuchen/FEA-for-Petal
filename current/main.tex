%-------------------------------------------------------------------------------
% Guide to the use of the ATLAS LaTeX package
%-------------------------------------------------------------------------------
% Specify where ATLAS LaTeX style files can be found.
\newcommand*{\ATLASLATEXPATH}{latex/}
%-------------------------------------------------------------------------------
\documentclass[datetop=true,UKenglish,texlive=2016]{\ATLASLATEXPATH atlasdoc}

% Use biblatex and biber for the bibliography
\usepackage[backend=biber]{\ATLASLATEXPATH atlaspackage}
\usepackage{\ATLASLATEXPATH atlasbiblatex}

\usepackage{authblk}
\usepackage{\ATLASLATEXPATH atlasphysics}

% Files with references in BibTeX format
% \addbibresource{../atlas_latex.bib}
\addbibresource{../main.bib}
% Paths for figures
\graphicspath{{../../logos/}, {../figures/}}

% Shorthand for \phantom to use in tables
\newcommand{\pho}{\phantom{0}}
\newcommand{\BibTeX}{\textsc{Bib\TeX}}
\newcommand{\File}[1]{\texttt{#1}\xspace}
\newcommand{\Macro}[1]{\texttt{\textbackslash #1}\xspace}
\newcommand{\Option}[1]{\textsf{#1}\xspace}
\newcommand{\Package}[1]{\texttt{#1}\xspace}

% User defined comments
\newcommand\doctype{disable}
\newenvironment{DIFnomarkup}{}{}

% Extra Packages
\usepackage{multirow, makecell}
\usepackage{siunitx}
	\sisetup{range-phrase=\text{~to~}, range-units=single, group-separator = {,}}
\usepackage{glossaries}
\usepackage[\doctype, textsize=tiny, color=green!40]{todonotes}
\usepackage[version=3]{mhchem}
\usepackage[flushleft]{threeparttable}
\usepackage{chngcntr}
	\counterwithin{figure}{section}
	\counterwithin{table}{section}
	\counterwithin{equation}{section}
\usepackage{placeins}
\usepackage{cancel}
\usepackage{mathtools}
% \usepackage[utf8x]{inputenc}
% \usepackage[autostyle=false, style=english]{csquotes}
% \MakeOuterQuote{"}

% Acronym
\newacronym{TID}{TID}{total ionising dose}
\newacronym{BoL}{beginning-of-life}{beginning-of-life}
\glsunset{BoL}
\newacronym{EoL}{end-of-life}{end-of-life}
\glsunset{EoL}
\newacronym{FEA}{FEA}{Finite Element Analysis}
\newacronym{CFRP}{CFRP}{carbon-fibre reinforced polymer}
\newacronym{IR}{IR}{infrared radiation}
\newacronym{TM}{TM}{Thermo-Mechanical}
\newacronym{TRACI}{TRACI}{Multipurpose Refrigeration Apparatus for \ce{CO2} Investigation}
\newacronym{htc}{HTC}{heat transfer coefficient}
\newacronym{CoBra}{CoBra}{\ce{CO2} Branch Calculator}
\newacronym{PSD}{PSD}{power spectral density}
\newacronym{PCB}{PCB}{printed circuit board}
\newacronym{ABC}{ABC}{ATLAS Binary Chip}
\newacronym{EoS}{EoS}{End-of-Substructure Card}
\newacronym{HCC}{HCC}{Hybrid Controller Chip}
\newacronym{ITk}{ITk}{Inner Tracker}
\newacronym{HL-LHC}{HL-LHC}{High-Luminosity LHC}
\newacronym{AMAC}{AMAC}{Autonomous Monitor and Control Chip}

%-------------------------------------------------------------------------------
% Generic document information
%-------------------------------------------------------------------------------

% Author and title for the PDF file
\hypersetup{pdftitle={FEA on Petals},pdfauthor={Yu-Heng Chen}}

\AtlasTitle{Finite Elements Thermo-Mechanical Analysis on Strip Petals for the ATLAS Phase-II Upgrades}

\author{Yu-Heng~Chen, Kurt~Brendlinger, Sergio~Diez~Cornell, and Carlos~Lacasta~Llacer}
% \affil[1]{DESY, Hamburg and Zeuthen, Germany}
% \affil[2]{Instituto de Fisica Corpuscular (IFIC), Centro Mixto Universidad de Valencia - CSIC, Spain}

\AtlasVersion{\ATTeXLiveVersion}

\AtlasAbstract{%
In order to fully exploit the increased luminosity of the High-Luminosity LHC, the Phase-II upgrades of the ATLAS detector is planed for 2023. A new \acrfull*{ITk} which consists of a pixel detector close to the beam line and a large area strip detector surrounding it has been designed and scheduled to be installed in 2025 for the central tracking system. The two endcaps which cover the forward region of the strip detector, each consist of 6 disks perpendicular to the beam axis with 32 petals arranged radially around each disk. The main aim of this research is first to validate the \acrfull*{FEA} approach to the thermo-mechanical performance of a strip petal in terms of rigidity, stability and heat transport and further to optimise the design. Several discrepancies between the \acrshort*{FEA} and the experiment are observed, their possible causes and indications are discussed, and then suggestions to related future studies are made. It is also found that we are facing the risk of a DC/DC converter overload, and hence multiple design changes including a parallel DC/DC converter system in certain modules and cooling pipe re-routing in order to improve the thermal performance are proposed.}

%-------------------------------------------------------------------------------
% This is where the document really begins
%-------------------------------------------------------------------------------
\begin{document}

\maketitle
\tableofcontents
\clearpage
\section{Introduction}
\input{Introduction}
\clearpage

\section{Strip Petals}
\input{StripPetals}
\clearpage

\section{Mechanical Analysis}
\input{MechanicalAnalysis}
\clearpage

\section{Thermal Analysis}
\input{ThermalAnalysis}
\clearpage

\section{Summary}
\input{Summary}
\section*{Acknowledgements}
\input{Acknowledgements}
\appendix
\clearpage
\section{Sandwich Theory} \label{app:Sandwich Theory}
\input{SandwichTheory}
\clearpage
\section{Thermal Runaway in Silicon Detectors} \label{app:Thermal Runaway}
\input{ThermalRunaway}
\clearpage
\section{Three Point Bending Test Data} \label{app:Three Point Bending Test Data}
\input{ThreePointBendingData}

\clearpage
\printbibliography

\begin{DIFnomarkup}
\ifnum \pdfstrcmp{\doctype}{draft}=0
\clearpage
\section[General Comments]{\color{gray}General Comments}
\input {GeneralComments} % Put a space between \input and its arguments. I think this should be seen as identical by latex but will prevent latexdiff's pattern matching from finding it (in current version always, in future versions only if --allow-spaces option is not selected)
\fi
\end{DIFnomarkup}

\end{document}
