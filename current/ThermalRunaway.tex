Recall that the three major factors causing the increase of power consumption are:
\begin{enumerate}
	\item \gls*{TID}-induced digital current increase of \gls*{ABC} and \gls*{HCC} chips
	\item The inefficiency of the FEAST DC/DC converter
	\item The sensor leakage current
\end{enumerate}
In what follows, we review how to model and parameterise these three elements. We then use them in a linear thermal-electric model to predict the temperature and power consumption of a full endcap based on \cite{ref:beck2010analytic,ref:Viehhauser2017}. More details about adapting this model to our case can be found in \cite{ref:Brendlinger2017}.
\subsection{Parametrisation}
\subsubsection*{TID-induced digital current increase of ABC and HCC chips}
It is already well-understood that \gls*{TID} typically gives rise to digital current increase of the CMOS transistors fabricated at the \SI{130}{\nano\meter} technology nodes, including the \gls*{HCC} and \gls*{ABC} chips, and governs the thermal behaviour of silicon detectors. As shown in Figure~\ref{fig:ABC Current Increase vs TID}, the increase for an \gls*{ABC}, for example, also depends on the dose rate and temperature during irradiation. The current increase factor can be factorized into two parts:
\begin{equation}
\mathrm{S(\mathbf{T}emperature,\mathbf{d}oserate,\mathbf{D}ose)=S_{overall}(\mathbf{T},\mathbf{d})}\times \mathrm{S_{shape}(\mathbf{D}}),
\end{equation}
where
\begin{flalign}
{}&\mathrm{S_{shape}(\mathbf{D})}=\max\lbrace(1-e^{-1.8(\mathbf{D}-400)/1000})-(1-e^{-0.4(\mathbf{D}-400)/1000}),0\rbrace&& \text{ with $\mathrm{\mathbf{D}}$ in \si{\kilo Rad/cm^2}}\\
{}&\mathrm{S_{overall}(\mathbf{T}, \mathbf{d})}=1+ae^{b(\SI{20}{\celsius}-\mathrm{\mathbf{T}})}\mathrm{\mathbf{d}}^c&& \text{ with $\mathrm{\mathbf{T}}$ in \si{\celsius} and $\mathrm{\mathbf{d}}$ in \si{\hour/{\kilo Rad}}}
\end{flalign}
The following fit parameters are obtained from fit to the \gls*{ABC} current data at the \gls*{TID} peak excluding high dose rate points:
\begin{equation}
\begin{aligned}
a={}&\SI{0.38201}{[\kilo Rad/\hour]^{\mathit{-c}}}\\
b={}&\SI{0.0245617}{[\celsius^{-1}]}\\
c={}&\SI{0.287121}{}\\
\end{aligned}
\end{equation}
In the absence of a \gls*{TID} profile of the \gls*{HCC} chips at different dose rates and temperatures, we now use the same scale factor for the \glspl*{HCC} and \glspl*{ABC} until more data becomes available.
\begin{figure}[!h]
\centering
\includegraphics[width=.6\textwidth]{../figures/ThermalRunaway/DigCurr.png}
\caption{\label{fig:ABC Current Increase vs TID}Digital current vs. \gls*{TID} for \gls*{ABC} chips during X-rays irradiations at different dose rates and temperatures~\cite{ref:Collaboration:2257755}. The digital current reaches a maximum at a \gls*{TID} level of \si{MRad} dependent on the dose rate and temperature, which is referred to as the \gls*{TID} peak.}
\end{figure}
\FloatBarrier
\subsubsection*{The Inefficiency of the FEAST DC/DC Converter}
The FEAST conversion efficiency $\mathcal{E}$ depends on the load current $\mathrm{\mathbf{I}}$ and temperature $\mathrm{\mathbf{T}}$ and is critical to the heat dissipation. This is parametrised as:
\begin{equation}
\mathcal{E}(\mathrm{\mathbf{I}},\mathrm{\mathbf{T}})=p_{0}+p_{1}\mathrm{\mathbf{I}}+p_{3}\mathrm{\mathbf{I}}^2+p_{4}\mathrm{\mathbf{I}}^3-{0.02\mathrm{\mathbf{T}}}/{\SI{25}{\celsius}},
\end{equation}
with $p_{0}=\SI{58.0448}{\percent}$, $p_{1}=\SI{58.0448}{\percent/\A}$, $p_{3}=\SI{-12.4747}{\percent/\A^2}$, $p_{4}=\SI{1.40142}{\percent/\A^3}$ obtained from fit to the measurement as shown in Figure~\ref{fig:FEAST Efficiency} taken from \cite{ref:Viehhauser2017}.
\begin{figure}[!h]
\centering
\includegraphics[width=.6\textwidth]{../figures/ThermalRunaway/FEASTEff.png}
\caption{\label{fig:FEAST Efficiency}FEAST efficiency vs. load current at different temperatures~\cite{ref:Brendlinger2017}}
\end{figure}%
\FloatBarrier%
\subsubsection*{Sensor Leakage Current}
Irradiated silicon sensors generate a temperature-dependent leakage current and thus heat dissipation:
\begin{align}
\Delta P_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi},\mathrm{\mathbf{T}}})&=V_{\mathrm{bias}}i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}})A_{\mathrm{Sensor}}\\
&=V_{\mathrm{bias}}i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}}_{ref})\left[\frac{\mathrm{\mathbf{T}}_{ref}}{\mathrm{\mathbf{T}}}\right]^2\exp{\left[\frac{\SI{1.2}{\electronvolt}}{k_B}\left(\frac{1}{\mathrm{\mathbf{T}}_{ref}}-\frac{1}{\mathrm{\mathbf{T}}}\right)\right]}A_{\mathrm{Sensor}},
\end{align}
where $i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}}_{ref})$ is the leakage current density of the sensor measured at  reference temperature $\mathrm{\mathbf{T}}_{ref}=\SI{-15}{\celsius}$. Note that the leakage current density also depends on bias voltage $V_\mathrm{bias}$. Here we use $V_\mathrm{bias}=\SI{500}{V}$ as the nominal value and a linear fit to the data taken from \cite{ref:Viehhauser2017} gives
\begin{equation}
i_{\mathrm{Sensor}}(\mathrm{\mathbf{\Phi}},\mathrm{\mathbf{T}}_{ref}) = \cancelto{0}{-0.14}+3.40 \times 10^{-14} \mathrm{\mathbf{\Phi}},
\end{equation}
with $\mathrm{\mathbf{\Phi}}$ in \si{n_{eq}/m^2}.
Unlike \gls*{TID}-induced current increase, which relaxes when heading to \gls*{EoL} due to the rebound effect, sensor leakage current keeps increasing with increasing radiation. However, the 14-year lifetime is not long enough for it to make significant difference to the thermal behaviour compared with the increased power from the \gls*{TID}-induced digital current increase.
\FloatBarrier
\subsection{Thermo-electrical Relations}
The power consumption of individual component are given in \cite{ref:Viehhauser2017} and listed as follows:
\subsubsection*{Power in Module}
\begin{gather}
\begin{dcases}
I_\mathrm{ABC}(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=n_\mathrm{ABC}\left(S(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})I^\mathrm{digital}+I^\mathrm{analog}\right)\\
P_\mathrm{ABC}(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=V_\mathrm{hybrid}I_\mathrm{ABC}(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})
\end{dcases}\\
\begin{dcases}
I_\mathrm{HCC}(\mathrm{\mathbf{T}_{HCC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=n_\mathrm{HCC}\left(S(\mathrm{\mathbf{T}_{HCC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})I^\mathrm{digital}+I^\mathrm{analog}\right)\\
P_\mathrm{HCC}(\mathrm{\mathbf{T}_{HCC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})=V_\mathrm{hybrid}I_\mathrm{ABC}(\mathrm{\mathbf{T}_{HCC}},\mathrm{\mathbf{d}},\mathrm{\mathbf{D}})
\end{dcases}\\
P_\mathrm{AMAC}=V^{1.5}_\mathrm{AMAC}I^{1.5}_\mathrm{AMAC}+V^{3.3}_\mathrm{AMAC}I^{3.3}_\mathrm{AMAC}\\
\begin{aligned}
P_\mathrm{FEAST}(\mathrm{\mathbf{T}_{ABC}}, \mathrm{\mathbf{T}_{HCC}}, \mathrm{\mathbf{T}_{FEAST}}, d, D)=\left(V_\mathrm{FEAST}-V^{1.5}_\mathrm{AMAC}\right)I^{1.5}_\mathrm{AMAC}\left(V_\mathrm{FEAST}-V^{3.3}_\mathrm{AMAC}\right)I^{3.3}_\mathrm{AMAC}\\
{}+\left(P_\mathrm{ABC}(\mathrm{\mathbf{T}_{ABC}},d,D)+P_\mathrm{HCC}(\mathrm{\mathbf{T}_{HCC}},d,D)\right)\left[\frac{1}{\mathcal{E}(\mathrm{\mathbf{T}_{FEAST},I_\mathrm{FEAST}(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{T}_{HCC}},d,D))}}-1\right]
\end{aligned}\\
\begin{dcases}
\begin{aligned}
I_\mathrm{tape}(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{T}_{HCC}},\mathrm{\mathbf{T}_{FEAST}},d,D)={}&\left[P_\mathrm{ABC}(\mathrm{\mathbf{T}_{ABC}},d,D)+P_\mathrm{HCC}(\mathrm{\mathbf{T}_{HCC}},d,D)+P_\mathrm{AMAC}\right.\\
{}&{}\left.+P_\mathrm{FEAST}(\mathrm{\mathbf{T}_{ABC}}, \mathrm{\mathbf{T}_{HCC}}, \mathrm{\mathbf{T}_{FEAST}},d,D)\right]/V^\mathrm{in}_\mathrm{FEAST}
\end{aligned}\\
P_\mathrm{tape}(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{T}_{HCC}},\mathrm{\mathbf{T}_{FEAST}},d,D)=\left[n_\mathrm{modules}\times I_\mathrm{tape}(\mathrm{\mathbf{T}_{ABC}},\mathrm{\mathbf{T}_{HCC}},\mathrm{\mathbf{T}_{FEAST}},d,D)\right]^2R_\mathrm{tape}
\end{dcases}\\
P_\mathrm{RHV}(I_\mathrm{Sensor})=I_\mathrm{Sensor}^2R_\mathrm{HV}\\
P_\mathrm{HVMUX}=\frac{V_\mathrm{bias}^2}{R_\mathrm{HVMUX}+R_\mathrm{HV}}\\
P_\mathrm{HV}(I_\mathrm{Sensor})=P_\mathrm{RHV}(I_\mathrm{Sensor})+P_\mathrm{HVMUX}
\end{gather}

\subsubsection*{EoS Power}
\begin{align}
\begin{dcases}
\begin{aligned}
I_\mathrm{EoS}={}&n_\mathrm{GBTIA}\,I_\mathrm{GBTIA}+n_\mathrm{lpGBLD}\,I_{\mathrm{lpGBLD}_{2.5V}}\\
{}&{}+\frac{n_\mathrm{GBTIA}\,I_\mathrm{GBTIA}+n_\mathrm{lpGBLD}\,I_{\mathrm{lpGBLD}_{1.2V}}}{\mathcal{E}_{\mathrm{DCDC2}}(I_\mathrm{EoS},\mathrm{\mathbf{T}_{EoS}})}\times\frac{V_{\mathrm{EoS}_{1.2V}}}{V_{\mathrm{EoS}_{2.5V}}}
\end{aligned}\\
P_\mathrm{EoS}(\mathrm{\mathbf{T}_{EoS}})=\frac{V_{\mathrm{EoS}_{2.5V}}I_\mathrm{EoS}}{\mathcal{E}_{\mathrm{DCDC2}}(I_\mathrm{EoS},\mathrm{\mathbf{T}_{EoS}})}
\end{dcases}
\end{align}%
% \todo[color=green!40]{[Kurt] Maybe you can try to improve the spacing here?}
% \todo[color=gray!40]{How about like this?}%
Combining all these inputs, including irradiation profiles, power dissipation and thermal properties of the petal, we can predict its electrical and thermal behaviours over time, including, but not limited to, total power consumption, working temperature, electrical efficiency and headroom temperature, simultaneously. However, because the temperature and heat dissipation change are correlated, it cannot be solved in an analytical way. So far, this has been achieved in practice using a transient numerical approach. That is, for a given power consumption and temperature distribution at \gls*{BoL}, with a \SI{1}{month} time-step, the power consumption for the month are computed using the component temperatures from the preceding month, and then the component temperatures for the month are computed using the computed power consumption. This step is repeated sequentially to model the full lifetime of the detector.