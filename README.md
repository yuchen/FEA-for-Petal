| Version  | Resolution | Date |
|:---------|:-----------|:----:|
| Final Draft  | [[original](current/final.pdf)] [[low](current/final-small.pdf)]| 2018/03/16 |
| Latest   | [[original](current/main.pdf)] [[low](current/main-small.pdf)]| 2018/03/16 |
| Previous | [[original](before20171128/main.pdf)] [[low](before20171128/main-small.pdf)] | 2017/11/28 |
| Diff     | [[original](current/paperdiff.pdf)] [[low](current/paperdiff-small.pdf)] | |

* If you have any question/comment/suggestion, let me know by creating a new issue or in any way you like.
* The general comments and answers to these are listed in the last section of [Diff](current/paperdiff-small.pdf)