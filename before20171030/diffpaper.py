#!/usr/bin/env python
try:
    import sys
    from IPython.core.ultratb import AutoFormattedTB
    sys.excepthook = AutoFormattedTB()
except ImportError:
    print 'IPython is not installed. Colored Traceback will not be populated.'
import os
import threading
main = 'main.tex'
if not os.path.isfile(main):
    root = os.path.dirname(__file__)
    current = os.path.join(root, 'current')
    if os.path.isdir(current):
        os.chdir(current)
        if not os.path.isfile(main):
            raise IOError('"main.tex" does not exist.')
    else:
        raise IOError('"main.tex" does not exist.')

main_diff = 'main_diff.tex'
paperdiff = 'paperdiff.tex'
DOCTYPE_PATTERN = r'(\\newcommand.*\\doctype.*\{)(disable)(.*\}.*)'
LATEXDIFF_CMD = ["latexdiff", "--packages=siunitx,mhchem,glossaries,hyperref", os.path.join(os.path.pardir, "before20171023", main), main_diff, "--flatten"]
LATEXMK_CMD = ['latexmk', '-cd', '-pdf', '-synctex=1', paperdiff,  "-halt-on-error", "-pv"]
# LATEXMK_CMD = ["pdflatex", "-file-line-error", paperdiff]
# BIBER_CMD = ["biber", "paperdiff"]

def latexdiff(infname, outfname, pattern = DOCTYPE_PATTERN, compressed = True, compress_kwds = {}):
    # paperdiff_pdf = paperdiff.rstrip('.tex') + '.pdf'
    # if os.path.isfile(paperdiff_pdf):
    #     if os.stat(paperdiff_pdf).st_mtime >= max(os.stat(main).st_mtime, os.stat(os.path.join(os.path.pardir, "before20171023", main)).st_mtime):
    #         return
    import re
    prog = re.compile(pattern)
    prog_dcases = re.compile(r'.*?\\(?:begin|end)(\{dcases\}).*?\n')
    with open(infname) as inf:
        lines = inf.readlines()
    for i, l in enumerate(lines):
        doctyped = False
        if not doctyped:
            result = prog.search(l)
            if result:
                lines[i] = 'draft'.join(result.group(1,3)) + '\n'
                doctyped = True
            if 'begin{document}' in l and not l.lstrip().startswith('%'):
                doctyped = True
        else:
            lines[i] = prog_dcases.sub('dcases', l)

    with open(outfname, 'w') as outf:
        outf.writelines(lines)
    # return
    import subprocess
    with open('paperdiff.tex', 'w') as diff:
        subprocess.call(LATEXDIFF_CMD, stdout = diff)
    os.remove(outfname)
    for cmd in [LATEXMK_CMD]:#, BIBER_CMD, LATEXMK_CMD]:
        if subprocess.call(cmd) == 1:
            raise RuntimeError('Latex')
    if compressed:
        compress(paperdiff, **compress_kwds)

def compress(infname, prefix = '', suffix = '-small', outdirname = None):
    import subprocess
    dirname, fname = os.path.split(infname)
    if outdirname is None:
        outdirname = dirname
    fname, ext = os.path.splitext(fname)
    ext = '.pdf' # force input to be a *.pdf file
    infname = os.path.join(dirname, fname + ext)
    outfname = os.path.join(outdirname, prefix + fname + suffix + ext)
    if os.path.isfile(outfname):
        if os.stat(outfname).st_mtime >= os.stat(infname).st_mtime:
            return
    try:
        if subprocess.call(['gs',
                            '-sDEVICE=pdfwrite',
                            '-dCompatibilityLevel=1.4',
                            '-dPDFSETTINGS=/ebook',
                            '-dNOPAUSE',
                            '-dBATCH', '-sOutputFile=' + outfname,
                            fname + ext]) == 1:
            raise RuntimeError('Latex')
    except Exception:
        os.remove(outfname)

threading.Thread(target = latexdiff, args = (main, main_diff), kwargs = dict(pattern = DOCTYPE_PATTERN, compressed = True)).start()
threading.Thread(target = compress, args = (main,)).start()