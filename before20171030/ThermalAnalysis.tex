Thermal runaway in a silicon detector occurs in situations where the heat from the sensors and from the on-detector electronics are not balanced by their removal by the coolant~\cite{ref:Collaboration:2257755} with often detrimental effect to the devices. The power dissipation evolves over time due to efficiency drops of the DC/DC converters at high operating temperature, a \gls*{TID} induced digital current increase and sensor leakage power due to radiation damage. The power dissipation is expected to peak $\sim\SIrange{2}{3}{years}$ after the start of the detector operation as the effects of radiation damage become visible, an effect referred to as the \gls*{TID} peak and at \gls*{EoL}. The factors mentioned above together cause a special thermal behaviour in the silicon detector, which was studied and modelled in \cite{ref:beck2010analytic} and explained in Appendix~\ref{app:Thermal Runaway}. In this analysis, the FE model with power dissipation estimated at \gls*{BoL} is first validated with an \gls*{IR} test. The model is then used to study the thermal performance during the lifetime with the thermal behaviour taken into consideration and further used to optimise the design.

\subsection{Validation against Infrared Measurements}  \label{subsec:Validating with IR test}
\subsubsection*{Method Description}
An \gls*{IR} test is performed in order to validate the FE model. One caveat of this approach is that a silicon surface is highly reflective and its emissivity varies with temperature. To deal with this effect, instead of measuring the temperature of the silicon surface directly, we covered the silicon surfaces with black tapes of which the emissivity is known to be \SI{0.95}{} and measured the temperature from the black tapes in order to provide correct surface temperature. In addition, thermocouples are placed in certain regions (see right panel of Figure~\ref{fig:setup of IR test}) to give more precise measurement than IR measurement. The two most active power sources, the DC/DC converters in R2 and R3, are sheltered by black cover caps to prevent diffusive radiation; see right panel of Figure~\ref{fig:setup of IR test}. The setup is built around an existing \gls*{TRACI} and a dedicated climate chamber with an electric camera slider was built for this specific purpose. As shown in left panel of Figure~\ref{fig:setup of IR test}, the \gls*{TM} Prototype is hung on one side of the climate chamber and on the other side an \gls*{IR} camera is placed to measure the surface temperature. The lowest possible coolant temperature that can be achieved with this setup is \SI{-24}{\celsius}.

\begin{figure}[htb!]
\centering
\includegraphics[width=0.49\textwidth]{../figures/IRTest/IRsetup.jpg}
\includegraphics[width=0.49\textwidth]{../figures/IRTest/TMpetal_IRconfiguration_unpolished.png}
\caption{\label{fig:setup of IR test}\textbf{Left}: The front view of the setup for the IR test. The \gls*{IR} camera is placed in the foreground and the \gls*{TM} petal in the brackground. \textbf{Right}: \gls*{IR} configuration of the \gls*{TM} petal (unpolished side) with black tapes and black caps installed.} 
\end{figure}

\FloatBarrier
\subsubsection*{Results and Observations}
With this, the surface temperature of the \gls*{TM} petal is measured, and related environmental parameters are recorded at the same time. Both the experimental and the \gls*{FEA} results are shown in Figure~\ref{fig:results of IR test}.
In principle, their main features are consistent. However, it seems in the experiment the temperature is not only higher but also distributes more evenly in R3S0, R4S0 and R5S0 on the sensor surface, and the temperature difference goes as high as \SI{7.5}{\celsius} in the area of R4S0 over the pipe. As discussed in Table~\ref{tab:FEA_Conductivities}, the \gls*{htc} and the temperature of the coolant are crucial uncertainties in this study.
\begin{figure}[htb!]
\centering
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[height=0.27\textwidth]{../figures/IRTest/[Data][-24C][F]Full.pdf} &
\includegraphics[height=0.27\textwidth]{../figures/IRTest/[-24C][10Wm2C][F]Full-label_enlarged.png} \\
\includegraphics[height=0.27\textwidth]{../figures/IRTest/[Data][-24C][F]Sensor.pdf} &
\includegraphics[height=0.27\textwidth]{../figures/IRTest/[-24C][10Wm2C][F]Sensor-label_enlarged.png}
\end{tabular}
\caption{\label{fig:results of IR test}\textbf{Top}(\textbf{bottom})\textbf{-left}: apparent temperature of the full plane (markers-only region) of the front side of the \gls*{TM} petal with emissivity set to the emissivity of black tapes \SI{0.95}{} with $T_{\mathrm{TRACI}}=\SI{-24}{\celsius}$, $T_{\mathrm{inlet}}=\SI{-18}{\celsius}$, $T_{\mathrm{outlet}}=\SI{-22}{\celsius}$, $T_\mathrm{ambient}=\SI{13.7}{\celsius}$, $RH=\SI{2}{\%}$. \textbf{Top}(\textbf{bottom})\textbf{-right}: surface temperature of the full plane (sensors-only region) of the front side of the \gls*{TM} petal with $T_{\ce{CO2}}=\SIrange{-20}{-24}{\celsius}$, $\gls*{htc}_{\ce{CO2}}=\SIrange{4.9}{7.1}{kW.m^{-2}.K^{-1}}$, $T_\mathrm{ambient}=\SI{13.7}{\celsius}$, $\gls*{htc}_\mathrm{ambient}=\SI{5}{W.m^{-2}.K^{-1}}$}
\end{figure} \FloatBarrier
\subsubsection*{A Possible \ce{CO2} Dry-out}
One possibility is that \ce{CO2} dry-out occurs unexpectedly when entering R3S0, which reduces the \gls*{htc} and hence the cooling power down dramatically. This did not happen in the simulation (see Figure~\ref{fig:htc at the EoL}); however, in the simulation, not just the heat dissipation was assumed to distribute evenly, but neither the influence from the shape of the pipe, gravity nor pipe material was taken into consideration. It could be the case that one of these activated dry-out prematurely. To verify whether this is the case, we made an attempt to perform a better fit by fixing the \gls*{htc} to \SI{1.5}{W.m^{-2}.K^{-1}} within this region and tuning the linear function which stands for the coolant temperature to return the measured inlet\slash outlet temperature as depicted in Figure~\ref{fig:cooling scenarios}. The result is shown in Figure~\ref{fig:results of IR test tunned}. Note that the dry-out scenario is actually favoured by lower coolant temperature and therefore has better cooling effect so a higher $\gls*{htc}_\mathrm{ambient}=\SI{8}{W.m^{-2}.K^{-1}}$ is assumed. The temperature difference between the experiment and the simulation is less than \SI{2.5}{\celsius}. Although they are in good agreement, the main concern here is whether such a low outlet \ce{CO2} temperature is realistic. To answer this question, various approaches have been proposed and the easiest one may be simply flushing cold nitrogen instead of room-temperature nitrogen into the chamber to reduce the influence from the convection.
% Although indeed it performs an overall better fit, one can still be aware that the temperature of the hot area in R4S0 increases in the opposite of the measured value, let alone how high the surface temperature of R5S0 and R5S1 can be if dry out really happens. These show that this discrepancy is subject neither to dry out nor to any other vapor quality related scenarios.
\begin{figure}[htb!]
\centering
\includegraphics[width=0.9\textwidth]{../figures/IRTest/CoolingScenarios.pdf}
\caption{\label{fig:cooling scenarios}Depiction of the two different \ce{CO2} cooling scenarios: normal (solid lines) and dry-out (dotted lines). The $T_\mathrm{inlet}$ and $T_\mathrm{outlet}$ are fixed to \SI{-18}{\celsius} and \SI{-22}{\celsius} respectively.} 
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=0.9\textwidth]{../figures/IRTest/[-24C][10Wm2C][F]Sensor-Tunned-label_enlarged.png}
\caption{\label{fig:results of IR test tunned}Surface temperature of the full plane (sensors-only region) of the front side of the \gls*{TM} petal with $T_{\ce{CO2}}=\SIrange{-20}{-28}{\celsius}$, $\gls*{htc}_{\ce{CO2}}=\SIrange{4.9}{1.5}{kW.m^{-2}.K^{-1}}$, $T_\mathrm{ambient}=\SI{13.7}{\celsius}$, $\gls*{htc}_\mathrm{ambient}=\SI{8}{W.m^{-2}.K^{-1}}$.} 
\end{figure}

\clearpage
\subsection{Thermal Performance} \label{subsec:Thermal Performance during the lifetime}
For the \gls*{ITk} detector, the coolant temperature should range from \SIrange{-25}{-35}{\celsius} to meet the pixel detector cooling requirements. Two possible working points, \SI{-30}{\celsius} and \SI{-35}{\celsius}, are discussed at current stage. The \SI{-30}{\celsius} working point is considered in what follows in order to be consistent with the results from the strip barrel. Regarding the convection, the real tracker is fully enclosed on a room-temperature \ce{N2} cryostat with a flow of the order of \SI{1}{m^3/h}, which is extremely low compared to our laboratory environment. The surrounding \ce{N2} is so static that it settles to the temperature of the petal surface. The convective heat transfer is therefore considered to be negligible.~\cite{ref:SimulationsDetails}
\subsubsection*{At Beginning-of-life}
Using \textbf{PETAL-20170117/0} (see top panel of Figure~\ref{fig:[DP20170117/0] Model/S0}), the results of the simulation are shown in Figure~\ref{fig:[DP20170117/0] Model/S0}. First, it has good agreement with the previous \gls*{FEA} study in \cite{ref:Collaboration:2257755}. It is later found that the heat is very concentrated inside the central region of the sensor surface especially in R3 due to the lack of physical proximity of the central region to the cooling power. This appears to be improvable by rerouting the cooling pipe; more discussion can be found in \ref{subsec:Optimisation}.

\begin{figure}[htb!]
\centering
\begin{tabular}{m{0.3\textwidth}m{0.45\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=.75\textwidth]{../figures/IRTest/[20170117][DP0]Model.png}}\\
\includegraphics[height=0.15\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP0][F][20170117]Sensor.png} &
\multirow{2}{*}[0.4in]{\includegraphics[height=0.3\textwidth]{../figures/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S0.pdf}} \\
\includegraphics[height=0.15\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP0][B][20170117]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170117/0] Model/S0}\textbf{Top}: the front view of \textbf{PETAL-20170117/0}. \textbf{Bottom-left}: from top to bottom, the temperature distribution of the front and the back sensor surface. \textbf{Bottom-right}: box-whisker plot (min-[Q1-(mean)Q2-Q3]-max) of the temperature distribution of different devices in different regions.}
\end{figure}
\clearpage
\subsubsection*{During the whole lifetime}
Given that the power consumption and the power dissipation over the $\sim14$-year lifetime can be evaluated using the parametrisation explained in Appendix~\ref{app:Thermal Runaway}, the thermal behaviour of the petal of any time and any location can be predicted using \gls*{FEA}. However, since it is computing time consuming and considered to be unnecessary, the idea of an analytical thermal model is also developed in~\cite{ref:beck2010analytic,ref:Brendlinger2017}. Considering that the thermal paths can be modelled by analogy to an electrical circuit where heat flow is represented by current, temperatures are represented by voltages, heat sources by constant current sources, thermal resistances by resistors, it is sufficient to simplify the thermal circuit to the case that electronic devices connect to the sensor and the local support layer with a fixed thermal resistance $R_\mathrm{cm}$ in parallel and grounded to the cooling pipe at a constant temperature of \SI{-30}{\celsius}, a cold reservoir, as depicted in Figure~\ref{fig:TELinearModel}. On the big picture, the thermal resistances of each component type can be then extracted by fitting to averaged temperature increase versus injected power in \gls*{FEA} with different power injection scenarios based on the heat equation $\Delta T = P R$. The detailed procedure in practice is described as follows.\\
Currently, three different power injection scenarios listed below are put into the fit and the thermal resistances:
\begin{enumerate}
	\item All \glspl*{HCC} powered on, rest off: $P_\mathrm{HCC}=\SI{0.413}{W}$
	\item All \glspl*{ABC} powered on, rest off: $P_\mathrm{ABC}=\SI{0.149}{W}$
	\item All FEASTs powered on, rest off: $P_\mathrm{FEAST}=\SI{1.5}{W}$
\end{enumerate}

Relatively realistic power values are considered here since the thermal resistances are potentially sensitive to the power level. The simulation results are shown in Figure~\ref{fig:[DP20170117/0] Model/S1S2S3} separately. First, since all the branches where the corresponding component are not powered on can be treated as a open circuit in this simplified model, to meet the equi-temperature condition at the node, the heat equation for each power injection scenario can be written down as
\begin{small}
\begin{align}
\Delta T_{\mathrm{cm}}(P_{\mathrm{HCC}})  &= P_{\mathrm{HCC}} \times R_{\mathrm{cm}}   & &                         &=& \Delta T_{\mathrm{ABC}}(P_{\mathrm{HCC}})  &=&\Delta T_{\mathrm{FEAST}}(P_{\mathrm{HCC}})\\
\Delta T_{\mathrm{cm}}(P_{\mathrm{ABC}})  &= P_{\mathrm{ABC}} \times R_{\mathrm{cm}}   &=&\Delta T_{\mathrm{HCC}}(P_{\mathrm{ABC}})  & &                          &=&\Delta T_{\mathrm{FEAST}}(P_{\mathrm{ABC}})\\
\Delta T_{\mathrm{cm}}(P_{\mathrm{FEAST}})&= P_{\mathrm{FEAST}} \times R_{\mathrm{cm}} &=&\Delta T_{\mathrm{HCC}}(P_{\mathrm{FEAST}})&=& \Delta T_{\mathrm{ABC}}(P_{\mathrm{FEAST}})& &
\end{align}
\end{small}
$R_{\mathrm{cm}}$ can be therefore determined by a linear fit to temperature data of all devices. The result is shown in Figure~\ref{fig:TEFitting}. Note that since the physical proximity is of great concern because of the special geometry of the petal, it is done individually for each region. Moreover, since a large temperature variation within each region still appear in the data, a $\sim \SI{20}{\%}$ safety factor is applied to contain it. Then, the thermal resistance of the component that is powered on can be then solved directly by substituting $R_{\mathrm{cm}}$ into the equations:
\begin{align}
\Delta T_{\mathrm{HCC}}(P_{\mathrm{HCC}})&=P_{\mathrm{HCC}} \times (R_{\mathrm{cm}} + R_{\mathrm{HCC}})\\
\Delta T_{\mathrm{ABC}}(P_{\mathrm{ABC}})&=P_{\mathrm{ABC}} \times (R_{\mathrm{cm}} + R_{\mathrm{ABC}})\\
\Delta T_{\mathrm{FEAST}}(P_{\mathrm{FEAST}})&=P_{\mathrm{FEAST}} \times (R_{\mathrm{cm}} + R_{\mathrm{FEAST}})
\end{align}
The solutions are summarised in Table~\ref{tab:Summary of thermal impedances} along with the barrel result for a comparison. The thermal resistances of the components in a endcap module are generally quite comparable with those in a short barrel module except for the $R_\mathrm{FEAST}$. It is already understood to be caused by the faulty footprint of the FEAST of \SI[product-units=power]{3x3}{\mm} in our FE model as it should be \SI[product-units=power]{3.6x3.6}{\mm}. A factor of 1/1.44 is thus applied to the $R_\mathrm{FEAST}$ for a correction in all the following analysis. \\
After collecting all of these inputs including the thermal resistances, the power dissipation and the influence and dose profile, the thermal behaviour over time of the full endcap can be modelled. Here we use the S1.9+R2BP (Step 1.9 geometry with Run-2 beam pipe) \gls*{ITk} influence and dose profile from simulation \cite{ref:RadiationBackgroundSimulationsFullyInclinedAt4}. A danger of FEAST overload reveals itself when approaching the \gls*{TID} peak in the analysis because of the elevated load current from the digital current increase of \glspl*{ABC} and \glspl*{HCC} and may lead to thermal runaway. A load current of \SI{4.8}{A} in R3 is observed at the \gls*{TID}, which is above the maximum load current of \SI{4}{A} that is considered sustainable over long periods. It is therefore suggested to add an extra DC/DC block to the power board in R3 to reduce the current going through a single FEAST by half. A crude FE model \textbf{PETAL-20170117/1} as shown in Figure~\ref{fig:[DP20170117/1] Model/S0} is built for this purpose by simply adding only a FEAST to the existing power board in R3 as other components like\gls*{AMAC} are not expected to have a strong impact on the thermal performance. Although the final design is not yet determined, it is guaranteed that the final design will differ from the model significantly in the placement and card dimension after taking wire routing into consideration, but this is again expected not to affect the thermal performance significantly. More discussion about the specification of the future power board in R3 can be found in~\cite{ref:StripModuleSpecification}. \\
The simulation results at \gls*{BoL} are shown in Figure~\ref{fig:[DP20170117/0-1] Sim/S0}. As one can see, a major difference only appears at their FEAST temperatures in R3. As the power dissipation is divided among the two FEASTs for \textbf{PETAL-20170117/1}, the temperature rise is reduced by a factor of two, which can be equivalently understood as that a 2-FEAST system has half as much effective thermal resistance as an 1-FEAST system, \SI{17.550}{K/W} versus \SI{29.333}{K/W} from the fits. This also benefits the conversion efficiency significantly by 30\%. The influence of adapting a 2-FEAST system in R3 on load current and conversion efficiency are shown in Figure~\ref{fig:2FEASTCurrent} and full endcap results in Figure~\ref{fig:2FEASTFull}. Besides, one should also be aware of a similar risk of load current going up to \SI{3.6}{A} in R1 although the possibility is rather low so that it is decided not to change the design unless further studies prove its urgency.

\begin{figure}[htb!]
\includegraphics[width=\textwidth]{../figures/IRTest/TELinearModel.png}
\caption{\label{fig:TELinearModel}Depiction of the thermal circuits of \glspl*{ABC}, \glspl*{HCC},  FEASTs and \gls*{EoS} in the simplified analytic thermal model \cite{ref:Brendlinger2017}. $P_\mathrm{FEAST}$, $P_\mathrm{HCC}$, $P_\mathrm{ABC}$ or $P_\mathrm{EoS}$ shows the power consumption of each component while $R_\mathrm{FEAST}$, $R_\mathrm{HCC}$, $R_\mathrm{ABC}$, $R_\mathrm{EoS}$, $R_\mathrm{c}$ or $R_\mathrm{m}$ shows the thermal resistance of each component. Note that $R_\mathrm{FEAST}$, $R_\mathrm{HCC}$, $R_\mathrm{ABC}$ or $R_\mathrm{EoS}$ are not just the thermal resistances of the devices themselves but are associated with their unique thermal paths through something like PCBs for instance.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{ccc}
\includegraphics[width=0.32\textwidth]{../figures/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S1.pdf} & 
\includegraphics[width=0.32\textwidth]{../figures/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S2.pdf} & 
\includegraphics[width=0.32\textwidth]{../figures/IRTest/[FEA][20170117][-30C][0Wm2C][DP0][ALL]S3.pdf} 
\end{tabular}
\caption{\label{fig:[DP20170117/0] Model/S1S2S3}Box-whisker plot (min-[Q1-(mean)Q2-Q3]-max) of the temperature distribution of different devices in different regions for the three different power injection scenarios. \textbf{Left}: All \glspl*{ABC} powered on, rest off. $P_\mathrm{ABC}=\SI{0.149}{W}$. \textbf{Centre}: All \glspl*{HCC} powered on, rest off. $P_\mathrm{HCC}=\SI{0.413}{W}$. \textbf{Right}: All FEASTs powered on, rest off. $P_\mathrm{FEAST}=\SI{1.5}{W}$.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=\textwidth]{../figures/IRTest/TEFitting.png}
\caption{\label{fig:TEFitting}The component temperatures versus injected powers from \gls*{FEA} are displayed together with the linear fits with a safety factor of \SI{20}{\%} applied (black lines in blue areas) for the thermal resistances of each component in each region (from \textbf{left} to \textbf{right} and \textbf{top} to \textbf{bottom}, R0 to R5) \cite{ref:Brendlinger2017}. Different marker styles indicate different power injection scenarios and different colours indicate different components.}
\end{figure}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Summary of thermal impedances]{Summary of thermal resistance of each component in both endcap and barrel. This can be also found in~\cite{ref:Brendlinger2017}.}
\label{tab:Summary of thermal impedances}
{
\begin{tabular}{c l l l l}
Module & $R_{\mathrm{cm}}$ [\si{K/W}] & $R_{\mathrm{FEAST}}$ [\si{K/W}] & $R_{\mathrm{ABC}}$ [\si{K/W}] & $R_{\mathrm{HCC}}$ [\si{K/W}]\\
\toprule
\multicolumn{5}{c}{Endcap}\\
\midrule
R0 & 0.802 & 26.045 & 0.917 & 12.632\\
R1 & 0.991 & 28.256 & 0.671 & 12.744\\
R2 & 1.410 & 28.883 & 1.550 & 13.794\\
R3 & 0.873 & 29.333 & 0.566 &  6.812\\
R4 & 0.744 & 26.816 & 1.432 & 13.027\\
R5 & 0.596 & 27.450 & 1.034 & 13.177\\
\midrule
\multicolumn{5}{c}{Barrel}\\
\midrule
Short & 1.160 & 19.751 & 1.003 & 12.305\\
Long & 1.360 & 19.663 & 2.141 & 25.174\\
\midrule
\end{tabular}
}
\end{threeparttable}
\end{table}

\begin{figure}[htb!]
\centering
\includegraphics[width=.6\textwidth]{../figures/IRTest/FEASTCurrent.png}
\caption{\label{fig:FEASTCurrent}FEAST current increase each ring (with different colours) in each disk (with different line styles) over 14-year lifetime \cite{ref:Brendlinger2017}.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=\textwidth]{../figures/IRTest/[20170117][DP1]Model1.png}
\caption{\label{fig:[DP20170117/1] Model/S0}The front view of \textbf{PETAL-20170117/1}. The only difference from \textbf{PETAL-20170117/0} are the number of FEASTs in R3.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170117][-30C][0Wm2C][DP0-1][ALL]S0.pdf}
\caption{\label{fig:[DP20170117/0-1] Sim/S0}Box-whisker plot (min-[Q1-(mean)Q2-Q3]-max) of the temperature distribution of different devices in different regions for model \textbf{PETAL-20170117/1} along with \textbf{PETAL-20170117/0} for a comparison.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=0.48\textwidth]{../figures/IRTest/2FEASTCurrent.png}
\includegraphics[width=0.48\textwidth]{../figures/IRTest/2FEASTEff.png}
\caption{\label{fig:2FEASTCurrent}\textbf{Left}: the load current change of the 2-FEASTs scenario (red curve) versus 1-FEAST (black curve) in R3 in Disk 5 over 14-year lifetime. \textbf{Right}: the efficiency change of the 2-FEASTs (red curve) scenario versus 1-FEAST (black curve) in R3 in Disk 5 over 14-year lifetime \cite{ref:Brendlinger2017}.}
\end{figure}

\begin{figure}[htb!]
\centering
\includegraphics[width=\textwidth]{../figures/IRTest/2FEASTFull.png}
\caption{\label{fig:2FEASTFull}From \textbf{left} to \textbf{right}, the load current, the conversion efficiency and the temperature of FEASTs in each rings (with different colours) in each disks (with different line styles) over 14-year lifetime \cite{ref:Brendlinger2017}.}
\end{figure}

\clearpage
\subsection{Optimisation} \label{subsec:Optimisation}
Based on the results in Section~\ref{subsec:Thermal Performance during the lifetime}, a new design (\textbf{PETAL-20170704\slash0}) which has a pipe routing closer to the petal center has been developed in order to improve the thermal performance in terms of the temperature homogeneity on the sensor surface, as shown in Figure~\ref{fig:[DP20170704/0] Model/S0} along with its corresponding thermal simulation results. The overall maximum temperature is \SI{5}{\celsius} lower compared to the previous designs.\\
It is also desirable to optimise the positioning of the power boards so as to further cool FEASTs down. To this end, six different designs, including the baseline design \textbf{PETAL-20170704\slash0}, have been built and studied: \textbf{PETAL-20170704\slash0} to \textbf{PETAL-20170704\slash5}, based on the horizontal distance between the FEAST chips and the pipe, which is reduced in steps \SI{20}{\%} until for \textbf{PETAL-20170704\slash5} they sit directly above of the pipe. The results are listed in order from \textbf{PETAL-20170704\slash0} to \textbf{PETAL-20170704\slash5} in Figures~\ref{fig:[DP20170704/0] Model/S0}~to~\ref{fig:[DP20170704/5] S0}. It is however shown that moving their position toward the above of the pipe is not very helpful for cooling the FEASTs down. The simplified thermal model is further confirmed to be pertinent to its validity for the petal: the horizontal distance between the cooling pipe and the FEASTs is actually not very relevant. Instead, it is observed that after \textbf{PETAL-20170704/1} the temperatures start to increase, which is believed to be related to a subleading factor, the "heat-island effect" due to the crowded electronics. It is therefore shown that the best strategy would be putting the FEASTs in the central regions for this particular design, which is basically our baseline design. Although \textbf{PETAL-20170704\slash1} could be slightly better, the difference is too small to risk a design changing.
% TODO>>
\todo[color=blue!40,inline]{[Katharina] Are there plans to study convection? If so, this should be mentioned here.}
\todo[color=gray!40,inline]{I just try to emphasize that "the best strategy" suggested here depends on the how strong the convection is. Our understanding so far is that in the real detector environment, the convection can be considered negligible to first order. However, since the difference we discussed here is quite small so it probably makes some difference. In any case, the variation by chaning the position of FEASTs we discussed here is too small to make a difference in the big picture. I just removed it since it may not so important for the readers and may be misleading.}% TODO<<


\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=\textwidth]{../figures/IRTest/[20170704][DP0]Model.png}}\\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP0][F]Sensor.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP0][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP0][B]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/0] Model/S0}\textbf{Top}: the front view of \textbf{PETAL-20170704/0} for which the pipe routing is redesigned to be closer to the centeral region. Note that the positioning of the two FEASTs in R3 is for simulation purpose only and do not reflect to its actual design and the \glspl*{AMAC} are removed as well for simplicity. \textbf{Bottom-left}: from top to bottom, the temperature distribution of the front and the back sensor surface. \textbf{Bottom-right}: box-whisker plot (min-[Q1-(mean)Q2-Q3]-max) of the temperature distribution of different devices in different regions.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP1][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP1][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP1][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/1] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/1}.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP2][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP2][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP2][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/2] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/2}.}
\end{figure}

\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP3][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP3][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP3][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/3] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/3}.}
\end{figure}


\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP4][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP4][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP4][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/4] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/4}.}
\end{figure}


\begin{figure}[htb!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP5][F]Setup.png}}\\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP5][F]Sensor-label_enlarged.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP5][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP5][B]Sensor-label_enlarged.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/5] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/5}.}
\end{figure}