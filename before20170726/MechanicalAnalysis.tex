Environmental vibration is known to be a possible source of experimental errors---in some extreme case, source of damage to an instrument---especially when in strong resonance with the natural frequencies of the instrument. Violent vibration of the petals must be avoid during the experiment. Mechanical simulations aim to determine whether a particular design has natural frequencies away from the range of environmental vibration or is stiff enough to bear it. \\
For this purpose, at first, several lab-scale tests are being performed by either DESY ATLAS group with the Mechanical Prototype or previous researches; then these results are compared to the Mechanical FE model in order to validate and correct it. The validated \gls*{FEA} model can be then used to ensure that the mechanical performance of a new-designed petal fits our requirements.

\subsection{Validating with Three Point Bending Analysis}
Three point bending test is an economic and efficient way to start with to understand some basic elastic properties of the petal and validate FE model. We replicated the three point bending test introduced by the Valencia team~\cite{ref:SoldevilaSerrano:2257105} with the mechanical prototype manufactured by DESY: the petal is placed on two fixed cylindrical aluminum supports and a load is applied to the centre of mass. With a smartscope, the deflection out of plane $\delta_z$ is measured and with a strain gauge. A fully correlated quantity, longitudinal strain $\epsilon_x$, is also measured at the same time for a verification. In the \gls*{FEA} simulation, The setup is almost the same except that the load is given by a fixed external force uniformly spread through the area and with a magnitude of the weight of a given load. See Fig.~\ref{fig:setup of 3-point bending test}. An example of the static mechanical solutions with support span \SI{20}{cm} and load mass \SI{1250}{g} with ANSYS is shown in Fig.~\ref{fig:FEA deflections solution}.\\
Varying the support span L and the load P allows one to derive the Young's modulus of the co-cured facesheets $E_f$ and the core $G_c$ individually from the measured $\delta_z$ or $\epsilon_x$ based on the Sandwich Theory as described in Appendix~\ref{app:Sandwich Theory} to further locate the problem. The fittings to the measured/simulated deflections are shown in Fig.~\ref{fig:measured/simulated deflections}. It is worth mentioning that the deflections induced by the facesheet (first-order term) and the core (zero-order term) are very comparable in this span length range. That is to say, it is in principle very easy to disentangle $E_f$ and $G_c$ from the fitting point of view. Given that the fittings to the \gls*{FEA} are quite good as well, it can be concluded that in theory the geometric factor is negligible and one can rely on the Sandwich Theory in this case. However, in practice, the fittings to the measurements are not as the precision and the accuracy can be guaranteed, which indicates a possibility that something occurred during manufacturing or preserving. In Fig.~\ref{fig:young's modulus}, the derived Young's modulus are compared. Readers might be interested in why the Young's module of the core is much smaller than any of its compositions. It is however very reasonable due to the fact that they are not glued to each others, which makes the ensemble especially prone to bend. First of all, at the center hole $\mathsf{B \slash F_0}$ the measurement and \gls*{FEA} agree. Nevertheless, they are somehow highly inconsistent with both the results of Valencia and NIKHEF~\cite{ref:KoralToptop,ref:SoldevilaSerrano:2257105}. Ours is almost twice their measured deflections at the center hole in all the cases; thus roughly quarter the stiffness of the core. With slight shades of difference like shapes and compositions of the tested petals, it is still very questionable and led us to further cross-check. More data was taken through the hole close to the edge of core $\mathsf{B \slash F_2^+}$. The measured deflections at $\mathsf{B \slash F_2^+}$ are much smaller than $\mathsf{B \slash F_0}$, which is inconsistent with \gls*{FEA}. Even though this has never been considered or reported by other groups before, based on some fragmentary material, Valencia's \gls*{FEA} result as shown in Fig.~\ref{fig:FEA deflections solution from Valencia} for example, seemly it shouldn't happen in a normal case.\\
In short, the reason causing such a discrepancy between measurement, \gls*{FEA} and previous searches remains unknown. The closest guess to this may be the delamination between the bustapes and the \gls*{CFRP} facings, and indeed an visible "air bubble" caused by the delamination has already been found in the central region. It also explains why Valencia and NIKHEF didn't come across such an issue since the Mechanical Prototype is the very first prototype with bustapes co-cured. The difference between the measured values for the front side and for the back side of the petal is considered to be a circumstantial evidence on it; however how influential the "air bubble" is on the test is still a question in dispute. We are looking forward to answering this question with an incoming modal test as introduced in \ref{subsec:Modal Analysis} or a new mechanical prototype which will be manufactured in the very near future. 

\begin{figure}[ht!]
\centering
\begin{minipage}{0.24\textwidth}
\includegraphics[width=\textwidth]{../figures/3PointBending/Setupof3PointBending.jpg}
\end{minipage}
\begin{minipage}{0.24\textwidth}
\includegraphics[width=\textwidth]{../figures/3PointBending/Setupof3PointBending2.jpg}
\end{minipage}
\begin{minipage}{0.48\textwidth}
\includegraphics[width=\textwidth]{../figures/3PointBending/Setupof3PointBendingFEA.png}
\end{minipage}
\caption{\label{fig:setup of 3-point bending test}Left (center) panel: the front (side) view of the process of measuring deflections with a smartscope in a three point bending test. Right panel: The setup of three point bending test in \gls*{FEA}.}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{../figures/3PointBending/20cm1250g.png}
\caption{\label{fig:FEA deflections solution}Deflections out of plane obtained with the \gls*{FEA} model for the Petal structure, $L = \SI{20}{cm}$ and $P = \SI{1250}{g}$} 
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=\textwidth]{3PointBending/reduced_deflections.pdf}
\caption{\label{fig:measured/simulated deflections}The measured/simulated reduced deflections $\delta/PL$ versus square of span lengths $L^2$ are displayed together with the fitting results (solid lines) and their 95\% Confidence Intervals (shaded bands) to the data respectively using linear regression. Upper-left panel: measured reduced deflections at the center hole on the front side. Upper-center panel: measured reduced deflections at the center hole on the back side. Upper-right panel: simulated reduced deflections at the center hole on the front side. Lower-left panel: measured reduced deflections at the hole close to the core edge on the front side. Lower-center panel: measured reduced deflections at the hole close to the core edge on the back side. Lower-right panel: simulated reduced deflections at the hole close to the core edge on the front side.} 
\end{figure}


\begin{figure}[ht!]
\centering
\begin{minipage}{.48\textwidth}
\includegraphics[width=\textwidth]{../figures/3PointBending/YoungsModulus.pdf}
\caption{\label{fig:young's modulus}The comparison of the derived Young's modulus of different measurements and simulations together with the Young's modulus of the main components.} 
\end{minipage}
\begin{minipage}{.48\textwidth}
\includegraphics[width=\textwidth]{../figures/3PointBending/45cm1250gfromValencia.jpg}
\caption{\label{fig:FEA deflections solution from Valencia}Deflections out of plane obtained with the \gls*{FEA} model for the Petal structure, $L = \SI{45}{cm}$ and $P = \SI{1250}{g}$ from \cite{ref:SoldevilaSerrano:2257105}.}
\end{minipage}
\end{figure}

% \begin{minipage}{.5\textwidth}
% \begin{figure}[ht!]

% \end{figure}
% \end{minipage}