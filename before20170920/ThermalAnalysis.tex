Thermal runaway in the silicon detector, as described in Appendix~\ref{app:Thermal Runaway}, occurs in situations where the heat from the sensors together with the heat from the on-detector electronics is not balanced by its removal by the coolant~\cite{ref:Collaboration:2257755,ref:beck2010analytic}, often being devastating. As sensor power grows with radiation damage over the operation time, it will be extremely problematic at the \gls*{TID} peak and the \gls*{EoL}. Similarly, the FE model is first validated with an \gls*{IR} test and then used to understand the thermal performance and to optimize the design.

\subsection{Validating with IR test}  \label{subsec:Validating with IR test}
An \gls*{IR} test is performed in order to validate the FE model. One caveat of this approach is that a silicon surface is highly reflective and its emissivity varies with temperature. To deal with it, instead of measuring the temperature of the silicon surface directly, we covered the silicon surfaces with black tapes of which the emissivity is known to be \SI{0.95}{} and measured the temperature from the black tapes in order to provide correct surface temperature. In addition, several thermocouples are also placed to give more precise measurement in certain regions; the two most active power sources, DCDC converters in R2 \& R3, are sheltered by black cover caps in preventing diffusive radiation; see right panel of Figure~\ref{fig:setup of IR test}. The setup is built around an existing \gls*{TRACI} and a dedicated climate chamber with an electric camera slider was built for this specific purpose. As shown in left panel of Figure~\ref{fig:setup of IR test}, the \gls*{TM} Prototype is hang on one side of the climate chamber and on the other side an \gls*{IR} camera is placed to measure the surface temperature. The lowest possible coolant temperature that we can achieve with this setup is \SI{-24}{\celsius}, measured at \gls*{TRACI}. With this, we measured the surface temperature of the \gls*{TM} petal, and recorded related environmental parameters in the same time. Both the experimental and the \gls*{FEA} results are shown in Figure~\ref{fig:results of IR test}.
In principle, the main features of them are consistent. However, it seems in the experiment the temperature is not only higher but also distributes more evenly in R3S0, R4S0 and R5S0 on the sensor surface, and the temperature difference goes as high as \SI{7.5}{\celsius} in the area of R4S0 over the pipe. As discussed in Table~\ref{tab:FEA_Conductivities}, the \gls*{htc} and the temperature of the coolant are crucial uncertainties in this study. One possibility is that \ce{CO2} dry-out occurs unexpectedly when entering R3S0, which brings the \gls*{htc} thus cooling power down dramatically. This didn't happen in the simulation with \gls*{CoBra}; however, in the simulation, not just the heat dissipation was assumed to distribute evenly, but neither the influence from the shape of the pipe, gravity nor pipe material was taken into consideration. It could be the case that one of these activated dry-out prematurely. To verify whether it is the case, we made an attempt to perform a better fit by fixing the \gls*{htc} to \SI{1.5}{W.m^{-2}.K^{-1}} within this region and tuning the linear function which stands for the coolant temperature to give out the measured inlet\slash outlet temperature as depicted in Figure~\ref{fig:cooling scenarios}, and the result is performed in Figure~\ref{fig:results of IR test tunned}. Note that the dry-out scenario is actually benefited from lower coolant temperature and therefore has better cooling effect so stronger convection with ambient is assumed to compensate the extra cold. The temperature difference between the experiment and the simulation is less than \SI{2.5}{\celsius}. Although they are in good agreement, the main concern here is that "is it possible that the outlet \ce{CO2} temperature goes to such low?" To answer it, various approaches have been proposed and the easiest one may be simply flushing cold nitrogen instead of room-temperature nitrogen into the chamber to reduce the influence from the convection.
% Although indeed it performs an overall better fit, one can still be aware that the temperature of the hot area in R4S0 increases in the opposite of the measured value, let alone how high the surface temperature of R5S0 and R5S1 can be if dry out really happens. These show that this discrepancy is subject neither to dry out nor to any other vapor quality related scenarios.
 
\begin{figure}[ht!]
\centering
\includegraphics[width=0.49\textwidth]{../figures/IRTest/IRsetup.jpg}
\includegraphics[width=0.49\textwidth]{../figures/IRTest/TMpetal_IRconfiguration_unpolished.png}
\caption{\label{fig:setup of IR test}Left panel: The front view of the setup of the IR test. On the near side is the \gls*{IR} camera and on the far side is the \gls*{TM} petal. Right panel: \gls*{IR} configuration of the \gls*{TM} petal (unpolished side) with black tapes and black caps installed.} 
\end{figure}

\begin{figure}[ht!]
\centering
\begin{tabular}{m{0.5\textwidth}m{0.5\textwidth}}
\includegraphics[width=0.49\textwidth]{../figures/IRTest/[Data][-24C][F]Full.pdf} &
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-24C][10Wm2C][F]Full.png} \\
\includegraphics[width=0.49\textwidth]{../figures/IRTest/[Data][-24C][F]Sensor.pdf} &
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-24C][10Wm2C][F]Sensor.png} \\
\end{tabular}
\caption{\label{fig:results of IR test}Upper(lower)-left panels: apparent temperature of the full plane (markers-only region) of the front side of the \gls*{TM} petal with emissivity set to the emissivity of black tapes \SI{0.95}{} with $T_{\mathrm{TRACI}}=\SI{-24}{\celsius}$ $T_{\mathrm{inlet}}=\SI{-18}{\celsius}$ $T_{\mathrm{outlet}}=\SI{-22}{\celsius}$ $T_\mathrm{ambient}=\SI{13.7}{\celsius}$ $RH=\SI{2}{\%}$. Upper(lower)-right panels: surface temperature of the full plane (sensors-only region) of the front side of the \gls*{TM} petal with $T_{\ce{CO2}}=\SIrange{-20}{-24}{\celsius}$ $htc_{\ce{CO2}}=\SIrange{4.9}{7.1}{kW.m^{-2}.K^{-1}}$ $T_\mathrm{ambient}=\SI{13.7}{\celsius}$ $htc_\mathrm{ambient}=\SI{5}{W.m^{-2}.K^{-1}}$}
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=0.9\textwidth]{../figures/IRTest/CoolingScenarios.pdf}
\caption{\label{fig:cooling scenarios}Depiction of the two different \ce{CO2} cooling scenarios: normal (solid lines) and dry-out (dotted lines). The $T_\mathrm{inlet}$ and $T_\mathrm{outlet}$ are fixed to \SI{-18}{\celsius} and \SI{-22}{\celsius} respectively.} 
\end{figure}

\begin{figure}[ht!]
\centering
\includegraphics[width=0.9\textwidth]{../figures/IRTest/[-24C][10Wm2C][F]Sensor-Tunned.png}
\caption{\label{fig:results of IR test tunned}Surface temperature of the full plane (sensors-only region) of the front side of the \gls*{TM} petal with $T_{\ce{CO2}}=\SIrange{-20}{-28}{\celsius}$ $htc_{\ce{CO2}}=\SIrange{4.9}{1.5}{kW.m^{-2}.K^{-1}}$ $T_\mathrm{ambient}=\SI{13.7}{\celsius}$ $htc_\mathrm{ambient}=\SI{8}{W.m^{-2}.K^{-1}}$.} 
\end{figure}
\subsection{Thermal Performance during the lifetime} \label{subsec:Thermal Performance during the lifetime}

\subsection{Optimisation}
It is also found from~\ref{subsec:Validating with IR test} and \ref{subsec:Thermal Performance during the lifetime} that for the previous designs the heat is very concentrated inside the central region mainly due to the pipe routing. A new design (\textbf{PETAL-20170704\slash0}) which has a pipe routing closer to the petal center has been developed in order to improve the thermal performance in terms of the temperature homogeneity on the sensor surface as shown in Figure~\ref{fig:[DP20170704/0] Model/S0} along with its corresponding thermal simulation results. The overall maximum value significantly decreases by \SI{2}{\celsius} comparing to the previous designs.\\
It is also desired to optimize the positioning of the power boards for the same reason why we 2 FEASTs in R3. To do so, six different designs, including the baseline design \textbf{PETAL-20170704\slash0}, have been made: \textbf{PETAL-20170704\slash0} to \textbf{PETAL-20170704\slash5} based on the horizontal distance between the FEAST chips and the pipe: every time we reduce \SI{20}{\%} of the distance until for \textbf{PETAL-20170704\slash5} they sit directly above of the pipe. The results are listed in order from \textbf{PETAL-20170704\slash0} to \textbf{PETAL-20170704\slash5} in Figures.~\ref{fig:[DP20170704/0] Model/S0}~to~\ref{fig:[DP20170704/5] S0}. It is however shown that moving their position toward the above of the pipe does not always help in terms of FEAST temperatures. On the contrary, after \textbf{PETAL-20170704/1}, they start increasing. A hand-waving explanation of this is that because the power boards acquire cooling power from the sensor surface, where the heat disperses quite evenly in terms of the temperature scale of the FEASTs thanks to the high transverse thermal conductivity of the \gls*{CFRP} layers, the horizontal distance between the cooling pipe and the FEASTs is actually not very relevant: the linear model is further confirmed to be pertinent to its validity for the petal. What takes place here is the sub-leading factor which comes from the "heat-island effect" due to the crowded electronics. It is shown that the best strategy would be putting the FEASTs in the central regions for this particular design, which is basically our baseline design. Although \textbf{PETAL-20170704\slash1} could be slightly better, the difference is too small to risk a design changing. Note that depending on the actual situation the convection can make a significant influence to the conclusion.


\begin{figure}[ht!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=\textwidth]{../figures/IRTest/[20170704][DP0]Model.png}}\\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP0][F]Sensor.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP0][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP0][B]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/0] Model/S0}Upper panel: the front view of \textbf{PETAL-20170704/0} for which the pipe routing is redesigned to be closer to the centeral region. Note that the positioning of the two FEASTs in R3 is for simulation purpose only and do not reflect to its actual design and the AMACs are removed as well for simplicity. Lower-left panel: from top to bottom, the temperature distribution of the front and the back sensor surface. Lower-right panel: box-whisker plot (min-[Q1-Q2-Q3]-max) of the temperature distribution of different devices in different regions.}
\end{figure}

\begin{figure}[ht!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP1][F]Sensor.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP1][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP1][B]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/1] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/1}.}
\end{figure}

\begin{figure}[ht!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP2][F]Sensor.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP2][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP2][B]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/2] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/2}.}
\end{figure}

\begin{figure}[ht!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP3][F]Sensor.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP3][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP3][B]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/3] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/3}.}
\end{figure}


\begin{figure}[ht!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP4][F]Sensor.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP4][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP4][B]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/4] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/4}.}
\end{figure}


\begin{figure}[ht!]
\begin{tabular}{m{0.4\textwidth}m{0.6\textwidth}}
\multicolumn{2}{c}{\includegraphics[width=\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP5][F]Setup.png}}\\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP5][F]Sensor.png} &
\multirow{2}{*}[0.8in]{\includegraphics[width=0.6\textwidth]{../figures/IRTest/[FEA][20170731][-30C][0Wm2C][DP5][ALL]S0.pdf}} \\
\includegraphics[width=0.4\textwidth]{../figures/IRTest/[-30C][0Wm2C][DP5][B]Sensor.png} & \\
\end{tabular}
\caption{\label{fig:[DP20170704/5] S0}The same as Figure~\ref{fig:[DP20170704/0] Model/S0} but for \textbf{PETAL-20170704/5}.}
\end{figure}