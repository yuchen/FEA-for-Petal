\subsection{Introduction}
In order to fully exploited the increased luminosity of the High-Luminosity LHC, the Phase-II upgrades of the ATLAS detector is planed for 2022. Part of this is the so-called Petalet Project. A strip petal is a basic building block of an strip endcape, the forwarding region of the ATLAS Inner Tracker Silicon Strip Detector. Each of the two strip endcaps consists of 6 disks with 32 petals. 6 different flavors of modules with either different sensor shape, hybrid device or power board design are designed for each ring of a petal instead of 1 for all in order to reduce the amount of dead material as well as improve the tracking performance of the Inner Tracker in spite of even harsher operating environment especially when it comes to the radiation level~\cite{ref:Collaboration:2257755}. The main aim of this research is to study both the mechanical and thermal performance of the petal and further to optimize them. Due to the complexity of the petal design involved the goal can be hardly achieved in an analytical way; therefore \gls*{FEA} simulation powered by ANSYS are utilized as the main approach.
\subsection{Prototypes}
So far, two prototypes of the strip petal have been manufactured for different purposes. One is the so-called Mechanical Prototype and the other is the \gls*{TM} Prototype. The Mechanical Prototype is just the core of the petal without any electronic devices on it, which is mainly used to understand the mechanical performance; while the \gls*{TM} Prototype has a real module for R0 and dummy modules for R1, R2, R3, R4 and R5, which is mainly used to understand the thermal performance. Figure~\ref{fig:Photos of T and TM Prototypes} shows the back views of the Mechanical and \gls*{TM} Prototypes. Note that there are slight differences between them because the design changed in this period. They are mainly used for the purpose of validating FE models in this study and technical details about mechanical properties, thermal properties and power inputs used as input to the \gls*{FEA} can be found in Table.~\ref{tab:FEA_MechanicalProperties}-~\ref{tab:PowerInputs} respectively.

\begin{figure}[H]
\centering
\includegraphics[width=0.48\textwidth]{../figures/StripPetal/MPrototype.jpg}
\includegraphics[width=0.48\textwidth]{../figures/StripPetal/TMPrototype.png}
\caption{\label{fig:Photos of T and TM Prototypes}Left panel: Back view of the Mechanical Prototype. Right panel: Back view of the \gls*{TM} Prototype.} 
\end{figure}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Mechanical properties used for strip detector \gls*{FEA}]{Mechanical properties used as input to the \gls*{FEA}}
\label{tab:FEA_MechanicalProperties}
{
\begin{tabular}{l l c c c l}
        %\multicolumn{3}{c}{} \\

    \bfseries Part or  & \bfseries Material & E                    & $\gamma$                & \bfseries Thickness   & \bfseries Comment  \\
    \bfseries Interface&                    & \bfseries [\si{GPa}] & \bfseries [-]           & \bfseries [mm]        &                    \\
    \toprule
    \multirow{2}{*}{Bus tape} & Polyimide/  & \multirow{2}{*}{2.50}&  \multirow{2}{*}{0.33}  & \multirow{2}{*}{0.17} &                    \\
                              & Cu/Al       &                      &                         &                       &                    \\\hline
    Bus to facing             & -           & \multicolumn{2}{c}{(idealised)}                & -                     & co-cured           \\\hline
    CFRP Facing               & 0-90-0 CFRP & 353.00               & 0.49                    & 0.15                  & \cite{ref:Poley2016}\\\hline
    \multirow{2}{*}{Facing to Foam} & Hysol 9396 + & \multicolumn{2}{c}{\multirow{2}{*}{(idealised)}}         & \multirow{2}{*}{0.1}  &                     \\
                                    & graphite powder &                                      &                       &                     \\\hline
    Honeycomb          & Nomex C2-4.8-32    & 0.16                 & 0.40                    & \multirow{3}{*}{~5 (core)} &                \\
    Closeouts          & PEEK               & 385.50               & 0.39                    &                       &                      \\
    Graphite Foam             & Allcomp, 2g.cm-3 & 0.29            & 0.30                    &                       &                      \\\hline
    \multirow{2}{*}{Foam to Pipe}  & Hysol 9396 +  & \multicolumn{2}{c}{\multirow{2}{*}{(idealised)}}           & \multirow{2}{*}{0.1}  &                     \\
                                  & graphite powder &                                        &                       &                     \\\hline
    Cooling Pipe & Titanium (grade 2)       & 175.00               & 0.36                    & 0.14-0.15 (wall)      & \SI{2}{mm} inner dia.\\

    \bottomrule
\end{tabular}
}
\end{threeparttable}
\end{table}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Thermal conductivities used for strip detector \gls*{FEA}]{Thermal properties used as input to the \gls*{FEA}~\cite{ref:Collaboration:2257755}}
\label{tab:FEA_Conductivities}
{
\begin{tabular}{l l c c l}
        %\multicolumn{3}{c}{} \\

    \bfseries Part or  & \bfseries Material & K$_{x}$/K$_{y}$/K$_{z}$ & \bfseries Thickness  & \bfseries Comment  \\
    \bfseries Interface&                &\bfseries [\si{W.m^{-1}.K^{-1}}]          & \bfseries [mm]       & \\
    \toprule
    \multirow{2}{*}{ASIC}         & \multirow{2}{*}{Silicon}& 191 (250K)   & \multirow{2}{*}{0.30} &  \\
                      &             & - 148 (300K) &      &   \\\hline
    ABC         & \multirow{2}{*}{UV cure glue} &\multirow{2}{*}{ 0.5 }& \multirow{2}{*}{0.08} &\multirow{2}{*}{ 50\% coverage} \\
    to Hybrid   &               &               &           &                   \\  \hline
    HCC         &UV cure glue   & \multirow{2}{*}{0.5}  & \multirow{2}{*}{0.08} & \multirow{2}{*}{75\% coverage}\\
    to Hybrid   &or silver epoxy&               &           & \\\hline
    Hybrid PCB & Cu/polyimide & 72 / 72 / 0.36 & 0.2 &  \\\hline
    Power PCB & Cu/polyimide & 120 / 120 / 3 & 0.3 & \\\hline
    PCB to sensor & FH5313 Epolite & 0.23 & 0.12 & 75\% coverage \\\hline
    \multirow{2}{*}{Sensor} & \multirow{2}{*}{Silicon} & 191(250K) - & \multirow{2}{*}{0.3} & \\
        & & 148(300K) & & \\\hline
    Sensor to Bus & DC SE4445 & 2.0 & 0.1 -  0.2 & 100\% coverage \\\hline
    \multirow{2}{*}{Bus tape} & Polyimide/ & 0.17 / 0.17 / & \multirow{2}{*}{0.24} & \\
        & Cu/Al & 0.17 & & \\\hline
    Bus to facing & - & (idealised) & - & co-cured \\\hline
    \multirow{2}{*}{CFRP Facing} & \multirow{2}{*}{0-90-0 CFRP} & \multirow{2}{*}{180/ 90 / 1} & \multirow{2}{*}{0.15} & K13C2U fibre, \\
        & & & &  45 g/m$^2$ \\\hline
    \multirow{2}{*}{Facing to Foam} & Hysol 9396 + & \multirow{2}{*}{1.0} & \multirow{2}{*}{0.1} & \\
         & graphite powder & & & \\\hline
    Graphite Foam & Allcomp, 2g.cm-3 & 30 & ~5 mm (core) & \\\hline
    \multirow{2}{*}{Foam to Pipe} & Hysol 9396 + & \multirow{2}{*}{1.0} & \multirow{2}{*}{0.1} & \\
         & graphite powder & & & \\\hline
    Cooling Pipe & Titanium (grade 2) & 16.4 & 0.14-0.15 (wall) & \SI{2}{mm} inner dia. \\\hline
    \multirow{2}{*}{Fluid film} & \multirow{2}{*}{Bi-phase CO$_2$} & \multicolumn{2}{c}{htc \SIrange{4.9}{7.1}{} (at BoL) \tnote{a}}  & simulated at \\
        & &  \multicolumn{2}{c}{[\si{kW.m^{-2}.K^{-1}}]} & \SI{-30}{\celsius} \\\hline
    \multirow{2}{*}{Convection} & \multirow{2}{*}{Air} & \multicolumn{2}{c}{htc \SIrange{0}{15}{}(\SI{13.7}{\celsius})}  & adjusted to \\
        & &  \multicolumn{2}{c}{[\si{W.m^{-2}.K^{-1}}]} &  match \\
    \bottomrule
\end{tabular}
}
\begin{tablenotes}
\item[a] In the simulation, due to the lack of real data of how the coolant temperature and \gls*{htc} distribute over the length of the pipe at \gls*{BoL}, the \gls*{htc} is obtained by NIKHEF with \gls*{CoBra} assuming that \gls*{TRACI} temperature will be \SI{-30}{\celsius} and total power will be \SI{67}{W} at \gls*{EoL} as shown in Figure~\ref{fig:htc at the EoL} within the range of \SIrange{4.9}{7.1}{kW.m^{-2}.K^{-1}}. Based on the understanding that the leakage power for the sensors is small comparing to the total power at \SI{-30}{\celsius}, the difference in the \gls*{htc} at the \gls*{BoL} and at the \gls*{EoL} is negligible. Moreover, the \gls*{htc} is assumed to be the same in the coolant temperature range of \SIrange{-24}{-30}{\celsius} for simplicity.
\end{tablenotes}
\end{threeparttable}
\end{table}
\begin{figure}[H]
\centering
\includegraphics[width=0.48\textwidth]{../figures/IRTest/3000fb-HTCvsLength.png}
\caption{\label{fig:htc at the EoL}\gls*{htc} along the pipe length at the \gls*{EoL}. Simulated at $T_{\mathrm{TRACI}}=\SI{-30}{\celsius}$ $P_{\mathrm{total}}=\SI{67}{W}$.} 
\end{figure}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Power Inputs used for strip detector \gls*{FEA}]{Nominal power dissipation values for the heat sources used as input to the \gls*{FEA}}
\label{tab:PowerInputs}
{
\begin{tabular}{l c l}
    \bfseries Device  & \bfseries Power Dissipation        & \bfseries Comment  \\
                      & \bfseries [\si{W}]                 &                    \\
    \toprule
    ABCN130           & 0.149							   & $\mathrm{I^{analog}}=\SI{0.055}{A}$ \\
                      &                                    & $\mathrm{I^{digital}}=\SI{0.033}{A}$\\\hline
    HCC               & 0.413                              &\\\hline
    DC-DC (R0)        & 1.117                              &\\
    DC-DC (R1)        & 1.315                              &\\
    DC-DC (R2)        & 0.941                              &\\
    DC-DC (R3)        & 1.936                              & \SI{0.968}{W} for each\\
                      &                                    & in the 2-FEASTs\\
                      &                                    & scenario\\
    DC-DC (R4)        & 1.067                              &\\
    DC-DC (R5)        & 1.067                              &\\\hline
    EoP               & 3.030                              &\\
    \bottomrule
\end{tabular}
}
\end{threeparttable}
\end{table}