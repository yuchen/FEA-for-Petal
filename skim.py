#!/usr/bin/env python
try:
    import sys
    from IPython.core.ultratb import AutoFormattedTB
    sys.excepthook = AutoFormattedTB()
except ImportError:
    print 'IPython is not installed. Colored Traceback will not be populated.'

import os
main = 'main.tex'
if not os.path.isfile(main):
    root = os.path.dirname(__file__)
    current = os.path.join(root, 'current')
    if os.path.isdir(current):
        os.chdir(current)
        if not os.path.isfile(main):
            raise IOError('"main.tex" does not exist.')
    else:
        raise IOError('"main.tex" does not exist.')

from glob import iglob
import os
exts = ['*.aux', '*.bbl', '*.toc', '*.run.xml', '*.bcf', '*.log', '*.fdb_latexmk', '*.synctex.gz*', '*.out', '*.fls', '*.blg']
for ext in exts:
	for f in iglob(ext):
		os.remove(f)
# LATEXMK_CLEAN_CMD = ['latexmk', '-c']
# import subprocess
# subprocess.call(LATEXMK_CLEAN_CMD)