#!/usr/bin/env python
try:
    import sys
    from IPython.core.ultratb import AutoFormattedTB
    sys.excepthook = AutoFormattedTB()
except ImportError:
    print 'IPython is not installed. Colored Traceback will not be populated.'
import os
import threading
main = 'main.tex'
if not os.path.isfile(main):
    root = os.path.dirname(__file__)
    current = os.path.join(root, 'current')
    if os.path.isdir(current):
        os.chdir(current)
        if not os.path.isfile(main):
            raise IOError('"main.tex" does not exist.')
    else:
        raise IOError('"main.tex" does not exist.')

main_diff = 'main_diff.tex'
paperdiff = 'paperdiff.tex'
TODO_PATTERN = r'(\\usepackage\[.*)(disable)(.*]\{todonotes\}.*)'
LATEXDIFF_CMD = ["latexdiff", "--packages=siunitx,mhchem,glossaries,hyperref", os.path.join(os.path.pardir, "before20170920", main), main_diff, "--flatten"]
PDFLATEX_CMD = ["pdflatex", "-file-line-error", paperdiff]
BIBER_CMD = ["biber", "paperdiff"]

def latexdiff(infname, outfname, pattern = TODO_PATTERN, compressed = True, compress_kwds = {}):
    import re
    prog = re.compile(pattern)
    with open(infname) as inf:
        lines = inf.readlines()
    for i, l in enumerate(lines):
        result = prog.search(l)
        if result:
            lines[i] = 'draft'.join(result.group(1,3)) + '\n'
            break
        if 'begin{document}' in l and not l.lstrip().startswith('%'):
            break
    with open(outfname, 'w') as outf:
        outf.writelines(lines)
    import subprocess
    with open('paperdiff.tex', 'w') as diff:
        subprocess.call(LATEXDIFF_CMD, stdout = diff)
    os.remove(outfname)
    for cmd in [PDFLATEX_CMD, BIBER_CMD, PDFLATEX_CMD]:
        if subprocess.call(cmd) == 1:
            raise RuntimeError('Latex')
    if compressed:
        compress(paperdiff, **compress_kwds)

def compress(infname, prefix = '', suffix = '-small', outdirname = None):
    import subprocess
    dirname, fname = os.path.split(infname)
    if outdirname is None:
        outdirname = dirname
    fname, ext = os.path.splitext(fname)
    ext = '.pdf' # force input to be a *.pdf file
    infname = os.path.join(dirname, fname + ext)
    outfname = os.path.join(outdirname, prefix + fname + suffix + ext)
    if os.path.isfile(outfname):
        if os.stat(outfname).st_mtime >= os.stat(infname).st_mtime:
            return
    try:
        if subprocess.call(['gs',
                            '-sDEVICE=pdfwrite',
                            '-dCompatibilityLevel=1.4',
                            '-dPDFSETTINGS=/ebook',
                            '-dNOPAUSE',
                            '-dBATCH', '-sOutputFile=' + outfname,
                            fname + ext]) == 1:
            raise RuntimeError('Latex')
    except Exception:
        os.remove(outfname)

threading.Thread(target = latexdiff, args = (main, main_diff), kwargs = dict(pattern = TODO_PATTERN)).run()
threading.Thread(target = compress, args = (main,)).run()