\subsection{Introduction}
In order to fully exploited the increased luminosity up to $\mathcal{L}=\SI{7.5e34}{cm^{-2}s^{-1}}$ of the \gls*{HL-LHC}, the Phase-II upgrades of the ATLAS detector is planned for 2023 (see Figure~\ref{fig:HL-LHC-plan}). A new \gls*{ITk} Detector which consists of a pixel detector at small radius close to the beam line and a large area strip detector surrounding it has been designed for the central tracking system for the ATLAS experiment as shown in Figure~\ref{fig:ITk}. The forward region of the strip detector covering the pseudorapidity range $1.2\leq|\eta|\leq2.8$, the two strip endcaps, each consists of 6 disks with 32 petals for each disk. The main aim of this research is to study both the mechanical and thermal performance of a strip petal and further to optimize them. Due to the complexity of the petal design involved the goal can be hardly achieved in an analytical way; therefore \gls*{FEA} simulation powered by ANSYS~\cite{ref:ANSYS} are utilized as the main approach.

\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth]{../figures/StripPetal/HL-LHC-plan.png}
\caption{\label{fig:HL-LHC-plan}Updated status of the LHC baseline programme including the HL-LHC run \cite{ref:Collaboration:2257755}.} 
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=0.48\textwidth]{../figures/StripPetal/ITkFromFurtherOut.png}
\includegraphics[width=0.48\textwidth]{../figures/StripPetal/HitsRZ_Half_InclBrl4_GMX.png}
\caption{\label{fig:ITk}Left panel: A visualisation of the ITk as implemented in the simulation framework. Right panel: Schematic layout of the ITk showing the coverage of the central (forward) region of the Pixel Detector in red (dark red) and Strip Detector in blue (dark blue). \cite{ref:Collaboration:2257755}} 
\end{figure}

\subsection{Strip Petal Layout}
Figure~\ref{fig:petal sketch} shows a schematic of a petal (version number: \textbf{PETAL-20151222}). A petal consists of sensor modules and a low mass sandwich structure called petal core which provides the mechanical rigidity, support for the modules and houses the common electrical, optical and cooling services. The petal core also provides the accurate alignment and fixation points through the three locators, which constrain the movement of the petal in $xyz$, $z\varphi$, and $z$ respectively for reducing the resolution loss due to environmental vibration. All the power and data links are channeled through an end-of-substructure card, which forms the interface to the off-detector electronics. 6 different types of modules with either different sensor shapes, hybrid devices, or power board are designed for each ring of a petal instead of one for all in order to reduce the amount of dead material as well as improve the tracking performance of the \gls*{ITk} in spite of the even harsher operating environment especially when it comes to the radiation level at which fluences up to \SI{1.2e15}{n_{eq}/cm^2} and \gls*{TID} up to \SI{50}{MRad}~\cite{ref:Collaboration:2257755}. A schematic of a module can be found in Figure~\ref{fig:module} and more details about the electronics above in Table~\ref{tab:PowerInputs}.

\begin{figure}[!htb]
\centering
\includegraphics[width=\textwidth]{../figures/StripPetal/Petal_sketch.png}
\caption{\label{fig:petal sketch}Top (Bottom) panel: External (Internal) structure of a petal.}
\end{figure}

\begin{figure}[!htb]
\centering
\includegraphics[width=0.6\textwidth]{../figures/StripPetal/ExplodedModule.png}
\caption{\label{fig:module}Exploded view of a module. This is actually a short-strip barrel module. However, petal modules feature the same component groups.}
\end{figure}

\subsection{Prototypes}
So far, two prototypes of the strip petal have been manufactured for different purposes. One is the so-called Mechanical Prototype and the other is the \gls*{TM} Prototype. The Mechanical Prototype is just the petal core without any electronic devices on it, which is mainly used to understand the mechanical performance; while the \gls*{TM} Prototype has a real module for R0 and dummy modules for R1, R2, R3, R4 and R5, which is mainly used to understand the thermal performance. Figure~\ref{fig:Photos of T and TM Prototypes} shows the back views of the Mechanical and \gls*{TM} Prototypes. Note that there are slight differences between them because the design changed in this period. They are mainly used for the purpose of validating FE models in this study. The technical details of mechanical properties, thermal properties and power inputs used as input to the \gls*{FEA} can be found in Tables~\ref{tab:FEA_MechanicalProperties}-~\ref{tab:PowerInputs}.



\begin{figure}[!htb]
\centering
\includegraphics[width=0.48\textwidth]{../figures/StripPetal/MPrototype.jpg}
\includegraphics[width=0.48\textwidth]{../figures/StripPetal/TMPrototype.png}
\caption{\label{fig:Photos of T and TM Prototypes}Left panel: Back view of the Mechanical Prototype. Right panel: Back view of the \gls*{TM} Prototype.} 
\end{figure}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Mechanical properties used for strip detector \gls*{FEA}]{Mechanical properties used as input to the \gls*{FEA}~\cite{ref:matweb2014material}.}
\label{tab:FEA_MechanicalProperties}
{
\begin{tabular}{l l c c c l}
        %\multicolumn{3}{c}{} \\

    \bfseries Part or  & \bfseries Material & E                    & $\gamma$                & \bfseries Thickness   & \bfseries Comment  \\
    \bfseries Interface&                    & \bfseries [\si{GPa}] & \bfseries [-]           & \bfseries [mm]        &                    \\
    \toprule
    \multirow{2}{*}{Bus tape} & Polyimide/  & \multirow{2}{*}{2.50}&  \multirow{2}{*}{0.33}  & \multirow{2}{*}{0.17} &                    \\
                              & Cu/Al       &                      &                         &                       &                    \\\hline
    Bus to facing             & -           & \multicolumn{2}{c}{(idealised)}                & -                     & co-cured           \\\hline
    CFRP Facing               & 0-90-0 CFRP & 353.00               & 0.49                    & 0.15                  & \cite{ref:Poley2016}\\\hline
    \multirow{2}{*}{Facing to Foam} & Hysol 9396 + & \multicolumn{2}{c}{\multirow{2}{*}{(idealised)}}         & \multirow{2}{*}{0.1}  &                     \\
                                    & graphite powder &                                      &                       &                     \\\hline
    Honeycomb          & Nomex C2-4.8-32    & 0.16                 & 0.40                    & \multirow{3}{*}{~5 (core)} &                \\
    Closeouts          & PEEK               & 385.50               & 0.39                    &                       &                      \\
    Graphite Foam             & Allcomp, 2g.cm-3 & 0.29            & 0.30                    &                       &                      \\\hline
    \multirow{2}{*}{Foam to Pipe}  & Hysol 9396 +  & \multicolumn{2}{c}{\multirow{2}{*}{(idealised)}}           & \multirow{2}{*}{0.1}  &                     \\
                                  & graphite powder &                                        &                       &                     \\\hline
    Cooling Pipe & Titanium (grade 2)       & 175.00               & 0.36                    & 0.14-0.15 (wall)      & \SI{2}{mm} inner dia.\\

    \bottomrule
\end{tabular}
}
\end{threeparttable}
\end{table}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Thermal conductivities used for strip detector \gls*{FEA}]{Thermal properties used as input to the \gls*{FEA}~\cite{ref:Collaboration:2257755}.}
\label{tab:FEA_Conductivities}
{
\begin{tabular}{l l c c l}
        %\multicolumn{3}{c}{} \\

    \bfseries Part or  & \bfseries Material & K$_{x}$/K$_{y}$/K$_{z}$ & \bfseries Thickness  & \bfseries Comment  \\
    \bfseries Interface&                &\bfseries [\si{W.m^{-1}.K^{-1}}]          & \bfseries [mm]       & \\
    \toprule
    \multirow{2}{*}{ASIC}         & \multirow{2}{*}{Silicon}& 191 (250K)   & \multirow{2}{*}{0.30} &  \\
                      &             & - 148 (300K) &      &   \\\hline
    ABC         & \multirow{2}{*}{UV cure glue} &\multirow{2}{*}{ 0.5 }& \multirow{2}{*}{0.08} &\multirow{2}{*}{ 50\% coverage} \\
    to Hybrid   &               &               &           &                   \\  \hline
    HCC         &UV cure glue   & \multirow{2}{*}{0.5}  & \multirow{2}{*}{0.08} & \multirow{2}{*}{75\% coverage}\\
    to Hybrid   &or silver epoxy&               &           & \\\hline
    Hybrid PCB & Cu/polyimide & 72 / 72 / 0.36 & 0.2 &  \\\hline
    Power PCB & Cu/polyimide & 120 / 120 / 3 & 0.3 & \\\hline
    PCB to sensor & FH5313 Epolite & 0.23 & 0.12 & 75\% coverage \\\hline
    \multirow{2}{*}{Sensor} & \multirow{2}{*}{Silicon} & 191(250K) - & \multirow{2}{*}{0.3} & \\
        & & 148(300K) & & \\\hline
    Sensor to Bus & DC SE4445 & 2.0 & 0.1 -  0.2 & 100\% coverage \\\hline
    \multirow{2}{*}{Bus tape} & Polyimide/ & 0.17 / 0.17 / & \multirow{2}{*}{0.24} & \\
        & Cu/Al & 0.17 & & \\\hline
    Bus to facing & - & (idealised) & - & co-cured \\\hline
    \multirow{2}{*}{CFRP Facing} & \multirow{2}{*}{0-90-0 CFRP} & \multirow{2}{*}{180/ 90 / 1} & \multirow{2}{*}{0.15} & K13C2U fibre, \\
        & & & &  45 g/m$^2$ \\\hline
    \multirow{2}{*}{Facing to Foam} & Hysol 9396 + & \multirow{2}{*}{1.0} & \multirow{2}{*}{0.1} & \\
         & graphite powder & & & \\\hline
    Graphite Foam & Allcomp, 2g.cm-3 & 30 & ~5 mm (core) & \\\hline
    \multirow{2}{*}{Foam to Pipe} & Hysol 9396 + & \multirow{2}{*}{1.0} & \multirow{2}{*}{0.1} & \\
         & graphite powder & & & \\\hline
    Cooling Pipe & Titanium (grade 2) & 16.4 & 0.14-0.15 (wall) & \SI{2}{mm} inner dia. \\\hline
    \multirow{2}{*}{Fluid film} & \multirow{2}{*}{Bi-phase CO$_2$} & \multicolumn{2}{c}{HTC \SIrange{4.9}{7.1}{} (at BoL) \tnote{a}}  & simulated at \\
        & &  \multicolumn{2}{c}{[\si{kW.m^{-2}.K^{-1}}]} & \SI{-30}{\celsius} \\\hline
    \multirow{2}{*}{Convection} & \multirow{2}{*}{Air} & \multicolumn{2}{c}{HTC \SIrange{0}{15}{}(\SI{13.7}{\celsius})}  & adjusted to \\
        & &  \multicolumn{2}{c}{[\si{W.m^{-2}.K^{-1}}]} &  match \\
    \bottomrule
\end{tabular}
}
\begin{tablenotes}
\item[a] In the simulation, due to the lack of real data of how the coolant temperature and \gls*{htc} change across the length of the pipe at \gls*{BoL}, the \gls*{htc} is obtained by NIKHEF with \gls*{CoBra} assuming a \gls*{TRACI} temperature of \SI{-30}{\celsius} and total power of \SI{67}{W} at \gls*{EoL} as shown in Figure~\ref{fig:htc at the EoL} within the range of \SIrange{4.9}{7.1}{kW.m^{-2}.K^{-1}}. Based on the understanding that the leakage power for the sensors is small compared to the total power at \SI{-30}{\celsius}, the difference in the \gls*{htc} at the \gls*{BoL} and at the \gls*{EoL} is negligible. Moreover, the \gls*{htc} is assumed to be the same in the coolant temperature range of \SIrange{-24}{-30}{\celsius} for simplicity. It is worth noting that for a bi-phase \ce{CO2} cooling system the temperature at the outlet is generally lower than at the inlet due to the bi-phase \ce{CO2} pressure drop.
\end{tablenotes}
\end{threeparttable}
\end{table}
\begin{figure}[!htb]
\centering
\includegraphics[width=0.8\textwidth]{../figures/IRTest/3000fb-HTCvsLength.png}
\caption{\label{fig:htc at the EoL}\gls*{htc} along the pipe length at the \gls*{EoL}. Simulated at $T_{\mathrm{TRACI}}=\SI{-30}{\celsius}$ $P_{\mathrm{total}}=\SI{67}{W}$.} 
\end{figure}

\begin{table}[htbp]
\centering
\begin{threeparttable}
\caption[Power Inputs used for strip detector \gls*{FEA}]{Nominal power dissipation values for the heat sources at the \gls*{BoL} used as input to the \gls*{FEA}~\cite{ref:sergio2017,ref:Collaboration:2257755}.}
\label{tab:PowerInputs}
{
\begin{tabular}{l l c l l}
    \bfseries Device & \bfseries Full Name  & \bfseries Heat [\si{W}] & \bfseries Basic functionality        & \bfseries Comment  \\
    \toprule
    ABC              & \multirow{3}{0.2\textwidth}{\footnotesize{ATLAS Binary Chip}}    & 0.149						  & \multirow{3}{0.2\textwidth}{\footnotesize{Converts incoming charge signal into hit information}} & $\mathrm{I^{analog}}=\SI{0.055}{A}$ \\
                    &                       &                             &                                                      & $\mathrm{I^{digital}}=\SI{0.033}{A}$\\
    &&&&\\\hline
    HCC & \multirow{2}{0.2\textwidth}{\footnotesize{Hybrid Controller Chip}}            & 0.413                       & \multirow{2}{0.2\textwidth}{\footnotesize{Interface between ABC and bustape}}                    &\\
    &&&&\\\hline
    FEAST (R0) & \multirow{8}{*}{FEAST} & 1.117 & \multirow{8}{0.2\textwidth}{\footnotesize{Synchronous Step-Down Buck DC/DC converter}}                              &\\
    FEAST (R1) &                        & 1.315 &                              &\\
    FEAST (R2) &                        & 0.941 &                              &\\
    \multirow{2}{*}{FEAST (R3)} &                        & \multirow{2}{*}{1.936} &                              & \footnotesize{\SI{0.968}{W} for each in the}\\
               &                        &       &                              & \footnotesize{2-FEASTs scenario}\\
    FEAST (R4) &                        & 1.067 &                              &\\
    FEAST (R5) &                        & 1.067 &                              &\\
    FEAST (EoS)&                        & 3.030 &                              &\\\hline
    AMAC & \multirow{2}{0.2\textwidth}{\footnotesize{Autonomous Monitor and Control Chip}} & assumed & \multirow{2}{0.2\textwidth}{\footnotesize{Provides monitoring and interrupt functionality}} & \\
               &                                                       & zero             &                                                 & \\

    \bottomrule
\end{tabular}
}
\end{threeparttable}
\end{table}