#!/usr/bin/env python
import sys
try:
    from IPython.core.ultratb import AutoFormattedTB
    sys.excepthook = AutoFormattedTB()
except ImportError:
    print 'IPython is not installed. Colored Traceback will not be populated.'
import os
import threading
import datetime
import glob
import logging
FORMAT = '%(asctime)s %(name)s[%(process)d] %(message)s'
logging.basicConfig(format=FORMAT, datefmt="%H:%M:%S")
logger = logging.getLogger('diff-paper')
try:
    import coloredlogs
    coloredlogs.install(logger = logger)
except ImportError:
    pass

logger.setLevel(logging.DEBUG)

main = 'main.tex'
if not os.path.isfile(main):
    root = os.path.dirname(__file__)
    current = os.path.join(root, 'current')
    if os.path.isdir(current):
        os.chdir(current)
        if not os.path.isfile(main):
            raise IOError('"main.tex" does not exist.')
    else:
        raise IOError('"main.tex" does not exist.')

main_diff = 'main_diff.tex'
previous = sorted(glob.iglob(os.path.join(os.pardir, 'before*')), reverse = True)[0].lstrip(os.path.pardir + os.path.sep)
paperdiff = 'paperdiff.tex'
DOCTYPE_PATTERN = r'(\\newcommand.*\\doctype.*\{)(disable)(.*\}.*)'
LATEXDIFF_CMD = ["latexdiff", "--packages=siunitx,mhchem,glossaries,hyperref", os.path.join(os.path.pardir, previous, main), main_diff, "--flatten"]
LATEXMK_CMD = ['latexmk', '-cd', '-pdf', '-synctex=1', "-halt-on-error", "-view=pdf", "-silent"]
# LATEXMK_CMD = ["pdflatex", "-file-line-error", paperdiff]
# BIBER_CMD = ["biber", "paperdiff"]

def write_README():
    README = \
"""| Version  | Resolution | Date |
|:---------|:-----------|:----:|
| Final Draft  | [[original](current/final.pdf)] [[low](current/final-small.pdf)]| {latest_date} |
| Latest   | [[original](current/main.pdf)] [[low](current/main-small.pdf)]| {latest_date} |
| Previous | [[original]({previous}/main.pdf)] [[low]({previous}/main-small.pdf)] | {previous_date} |
| Diff     | [[original](current/paperdiff.pdf)] [[low](current/paperdiff-small.pdf)] | |

* If you have any question/comment/suggestion, let me know by creating a new issue or in any way you like.
* The general comments and answers to these are listed in the last section of [Diff](current/paperdiff-small.pdf)"""\
.format(previous = previous,
        latest_date = format(datetime.datetime.fromtimestamp(os.stat('main.pdf').st_mtime), '%Y/%m/%d'),
        previous_date = '/'.join((previous[6:10], previous[10:12], previous[12:]))
        )
    return README

import subprocess
def latexmk(fname):
    return subprocess.call(LATEXMK_CMD + [fname])

def latexdiff(infname, outfname, pattern = DOCTYPE_PATTERN, compressed = True, compress_kwds = {}):
    # paperdiff_pdf = paperdiff.rstrip('.tex') + '.pdf'
    # if os.path.isfile(paperdiff_pdf):
    #     if os.stat(paperdiff_pdf).st_mtime >= max(os.stat(main).st_mtime, os.stat(os.path.join(os.path.pardir, "before20171023", main)).st_mtime):
    #         return
    import re
    prog = re.compile(pattern)
    # prog_dcases = re.compile(r'.*?\\(?:begin|end)(\{dcases\}).*?\n')
    with open(infname) as inf:
        lines = inf.readlines()
    for i, l in enumerate(lines):
        doctyped = False
        if not doctyped:
            result = prog.search(l)
            if result:
                lines[i] = 'draft'.join(result.group(1,3)) + '\n'
                doctyped = True
            if 'begin{document}' in l and not l.lstrip().startswith('%'):
                doctyped = True
        else:
            break
        #     lines[i] = prog_dcases.sub('dcases', l)

    with open(outfname, 'w') as outf:
        outf.writelines(lines)
    # return
    
    with open('paperdiff.tex', 'w') as diff:
        subprocess.call(LATEXDIFF_CMD, stdout = diff)
    os.remove(outfname)
    logger.info('First Try...')
    if latexmk(paperdiff):
            logger.critical('Fail when compiling. You can try to edit and fix it [Y/n]')
            if raw_input().lower().strip() == 'y':
                subprocess.call(['vim', diff.name])
                logger.info('Second Try...')
                if latexmk(paperdiff):
                    raise RuntimeError('Latex')
    if compressed:
        compress(paperdiff, **compress_kwds)

def compress(infname, prefix = '', suffix = '-small', outdirname = None):
    import subprocess
    dirname, fname = os.path.split(infname)
    if outdirname is None:
        outdirname = dirname
    fname, ext = os.path.splitext(fname)
    ext = '.pdf' # force input to be a *.pdf file
    infname = os.path.join(dirname, fname + ext)
    outfname = os.path.join(outdirname, prefix + fname + suffix + ext)
    if os.path.isfile(outfname):
        if os.path.isfile(infname):
            if os.stat(outfname).st_mtime >= os.stat(infname).st_mtime:
                return
    try:
        if subprocess.call(['gs',
                            '-sDEVICE=pdfwrite',
                            '-dCompatibilityLevel=1.4',
                            '-dPDFSETTINGS=/printer',
                            #-dPDFSETTINGS=/ebook',
                            '-dNOPAUSE',
                            #'-dUseCIEColor',
                            '-dBATCH', '-sOutputFile=' + outfname,
                            fname + ext]) == 1:
            raise RuntimeError('Latex')
    except Exception:
        os.remove(outfname)

if __name__ == '__main__':
    # latexdiff(main, main_diff, **dict(pattern = DOCTYPE_PATTERN, compressed = True))
    threading.Thread(target = latexdiff, args = (main, main_diff), kwargs = dict(pattern = DOCTYPE_PATTERN, compressed = True)).start()
    threading.Thread(target = compress, args = (main,)).start()
    threading.Thread(target = compress, args = ('final.tex',)).start()
    threading.Thread(target = compress, args = (paperdiff.replace('.tex', '.pdf'),)).start()
    with open('../README.md', 'w') as README:
        README.write(write_README())